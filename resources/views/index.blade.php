@extends('layouts.app')

@section('content')

<vue-carousel-slider></vue-carousel-slider>
<slick-slider
next-arrow-img="images/banner_arrow_right.svg"
prev-arrow-img="images/banner_arrow_left.svg"
></slick-slider>

@endsection
