@extends('email.default.main')

@section('content')
<table>
    <tbody>
        <tr>
            <td>問題:</td>
            <td>{{ $description }}</td>
        </tr>
        <tr>
            <td>附檔:</td>
            <td>{{ $image_id }}</td>
        </tr>
        <tr>
            <td>提問日期:</td>
            <td>{{ $create_time }}</td>
        </tr>
        <tr>
            <td>會員姓名:</td>
            <td>{{ $last_name }}{{ $first_name }}({{ $user_name }})</td>
        </tr>
        <tr>
            <td>會員編號:</td>
            <td>{{ $user_id }}</td>
        </tr>
        <tr>
            <td>email:</td>
            <td>{{ $user_email }}</td>
        </tr>
        <tr>
            <td>電話:</td>
            <td>{{ $user_phone }}</td>
        </tr>
        <tr>
            <td>手機資訊:</td>
            <td>{{ $model_type}} &nbsp;&nbsp; {{$version }}</td>
        </tr>
        <tr>
            <td>手機型號:</td>
            <td>{{ $model }}</td>
        </tr>
        <tr>
            <td>APP版本:</td>
            <td>{{ $app_version }}</td>
        </tr>
    </tbody>
</table>
@endsection

