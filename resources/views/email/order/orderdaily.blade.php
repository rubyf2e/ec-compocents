@extends('email.default.main')

@section('content')
<table>
	<tbody>
		<tr>
			<td>索取紙本發票統計：訂單 ＝> 聯式</td>
		</tr>
		@foreach ($dailyInvoiceCount as $order_no => $receipt_type)
		<tr>
			<td>{{ $order_no }} ＝> {{ $receipt_type }}</td>
		</tr>
		@endforeach
		<tr>
			<td></td>
		</tr>
		<tr>
			<td></td>
		</tr>
		<tr>
			<td>訂單統計：狀態 > 數量</td>
		</tr>
		@foreach ($dailyCount as $key => $row)
		<tr>
			<td>{{ $row['order_status'] }} > {{ $row['counter'] }} 筆</td>
		</tr>
		@endforeach
	</tbody>
</table>
@endsection
