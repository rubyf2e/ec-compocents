@extends('layouts.app')

@section('content')

<br>
<br>

<div class="container">
	<div class="row">
		<mpgfeedback status="{{$Status}}" message="{{$Message}}" tradeinfoarray="{{$TradeInfoArray}}"></mpgfeedback>
	</div>
</div>

@endsection
