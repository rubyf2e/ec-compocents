@extends('layouts.app')
@section('content')
<tap-pay-form></tap-pay-form>
@endsection


@section('js')
<script>
	var _TAPPAY_APP_ID      = "{{ config('custom._TAPPAY_APP_ID') }}";
	var _TAPPAY_APP_KEY     = "{{ config('custom._TAPPAY_APP_KEY') }}";
	var _TAPPAY_SERVER_TYPE = "{{ config('custom._TAPPAY_SERVER_TYPE') }}";
	var LocalUrl            = "{{ url('').'/' }}";
</script>
<script src="https://js.tappaysdk.com/tpdirect/v3"></script>
<script src="{{ mix('js/TapPay.js') }}"></script>
@endsection
