@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h1><div id ="state_change"></div></h1>
					<h3>
						目前房間人數：@{{ usersInRoom.length }}
					</h3>
				</div>

				<div class="chat-log-container clearfix">
					<chat-users-in-room  :users-in-room="usersInRoom"></chat-users-in-room>
					<chat-log :messages="messages" user-id="{{ Auth::user()->id }}" user-name="{{ Auth::user()->name }}"></chat-log>

				</div> 

			</div>
		</div>
	</div>
</div>

@endsection




