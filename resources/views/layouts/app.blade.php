<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <meta http-equiv="Pragma" content="no-cache" />

    <meta http-equiv="Cache-Control" content="no-cache" />

    <meta http-equiv="Expires" content="0" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ url('').mix('css/app.css') }}" rel="stylesheet">
    @yield('css')
    @yield('headJS')
</head>
<body>
    <div id="app">

        <nav class="navbar navbar-expand-sm navbar-dark bg-dark">

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample03" aria-controls="navbarsExample03" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarsExample03">
                <ul class="navbar-nav mr-auto">
                    <li><a class="nav-link" href="{{ url('/') }}">首頁</a></li>
                    <li><a class="nav-link" href="{{ url('/chatBot') }}">用來靠北的聊天室XDD</a></li>
                    @guest
                    <li><a class="nav-link active" href="{{ route('login') }}">Login</a></li>
                    <li><a class="nav-link" href="{{ route('register') }}">Register</a></li>
                    @else
                    <li class="nav-item dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
                @endguest

            </ul>
        </div>

        <div class="container shopping-cart">
            <div class="row">
             <shopping-cart cart-images="{{ url('images/cart.svg') }}"></shopping-cart>
         </div>
     </div>

 </nav>

 @yield('content')
</div>

<!-- Scripts -->
<script src="{{ url('').mix('js/manifest.js') }}"></script>
<script src="{{ url('').mix('js/vendor.js') }}"></script>
<script src="{{ url('').mix('js/app.js') }}"></script>
@yield('js')
</body>
</html>
