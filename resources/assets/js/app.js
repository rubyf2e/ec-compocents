require('./bootstrap');
import VueCarousel from 'vue-carousel';
import eventBus from './eventBus';
import { CONFIG as SiteConfig } from './config'
Object.defineProperty(Vue.prototype, '$conf', { value: SiteConfig });

Vue.use(VueCarousel);

Vue.component('TapPayForm', require('./components/TapPay/form.vue'));

Vue.component('SlickSlider', require('./components/Sliders/SlickSlider.vue'));
Vue.component('VueCarouselSlider', require('./components/Sliders/VueCarouselSlider.vue'));
Vue.component('products', require('./components/Products/ProductsPage.vue'));



Vue.component('shopping-name', require('./components/ShoppingInfo/ShoppingName.vue'));
Vue.component('shopping-recipient', require('./components/ShoppingInfo/ShoppingRecipient.vue'));
Vue.component('shopping-email', require('./components/ShoppingInfo/ShoppingEmail.vue'));
Vue.component('shopping-tel', require('./components/ShoppingInfo/ShoppingTel.vue'));
Vue.component('shopping-comment', require('./components/ShoppingInfo/ShoppingComment.vue'));
Vue.component('shopping-shipping', require('./components/ShoppingInfo/ShoppingShipping.vue'));
Vue.component('shopping-receipt', require('./components/ShoppingInfo/ShoppingReceipt.vue'));
Vue.component('shopping-mpgApi', require('./components/ShoppingInfo/ShoppingMpgApi.vue'));
Vue.component('shopping-list', require('./components/ShoppingInfo/ShoppingList.vue'));
Vue.component('shopping-listItem', require('./components/ShoppingInfo/ShoppingListItem.vue'));
Vue.component('mpgfeedback', require('./components/MpgFeedBack/MpgFeedBack.vue'));

Vue.component('shopping-info', require('./components/ShoppingInfo/ShoppingInfo.vue'));
Vue.component('shopping-cart', require('./components/ShoppingCart/ShoppingCart.vue'));
Vue.component('shopping-order', require('./components/ShoppingOrder/ShoppingOrder.vue'));
Vue.component('cart-item', require('./components/ShoppingCart/ShoppingCartItem.vue'));
Vue.component('order-item', require('./components/ShoppingOrder/ShoppingOrderItem.vue'));


Vue.component('chat-log', require('./components/Chat/ChatLog.vue'));
Vue.component('chat-composer', require('./components/Chat/ChatComposer.vue'));
Vue.component('chat-users-in-room', require('./components/Chat/ChatUsersInRoom.vue'));
Vue.component('chat-typing', require('./components/Chat/ChatTyping.vue'));


const app = new Vue({
    el: '#app',
    data: function() {
        return {
            messages: [],
            usersInRoom: [],
            bus: eventBus
        }
    },
    methods: {
        addMessage:function(message) {
            axios.post('/messages', message).then(response => {

            })
        }
    },
    created() {
       const self = this;
       axios.get(this.$conf.CHATBOT_FETCHMESSAGE).then(response => {
        self.messages = response.data;
    });


       Echo.join('ChatBot-chatroom')
       .here((users) => {
        this.usersInRoom = users;
    })
       .joining((user) => {
        this.usersInRoom.push(user);
    })
       .leaving((user) => {
        this.usersInRoom = this.usersInRoom.filter(u => u != user)
    });

       window.ChatBotChannel.bind('MessageSent', function(e) {      
          self.messages.push(e);
      });

   }

});
