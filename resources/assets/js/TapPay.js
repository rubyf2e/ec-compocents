TPDirect.setupSDK(_TAPPAY_APP_ID, _TAPPAY_APP_KEY, _TAPPAY_SERVER_TYPE)
TPDirect.card.setup({
	fields: {
		number: {
			element: '.form-control.card-number',
			placeholder: '**** **** **** ****'
		},
		expirationDate: {
			element: document.getElementById('tappay-expiration-date'),
			placeholder: 'MM / YY'
		},
		ccv: {
			element: $('.form-control.cvc')[0],
			placeholder: '後三碼'
		}
	},
	styles: {
		'input': {
			'color': 'gray'
		},
		'input.ccv': {
		},
		':focus': {
			'color': 'black'
		},
		'.valid': {
			'color': 'green'
		},
		'.invalid': {
			'color': 'red'
		},
		'@media screen and (max-width: 400px)': {
			'input': {
				'color': 'orange'
			}
		}
	}
})

TPDirect.card.onUpdate(function (update) {
	if (update.canGetPrime) {
		$('button[type="submit"]').removeAttr('disabled')
	} else {
		$('button[type="submit"]').attr('disabled', true)
	}

	let newType = update.cardType === 'unknown' ? '' : update.cardType
	$('#cardtype').text(newType)

	if (update.status.number === 2) {
		setNumberFormGroupToError('.card-number-group')
	} else if (update.status.number === 0) {
		setNumberFormGroupToSuccess('.card-number-group')
	} else {
		setNumberFormGroupToNormal('.card-number-group')
	}
	if (update.status.expiry === 2) {
		setNumberFormGroupToError('.expiration-date-group')
	} else if (update.status.expiry === 0) {
		setNumberFormGroupToSuccess('.expiration-date-group')
	} else {
		setNumberFormGroupToNormal('.expiration-date-group')
	}
	if (update.status.cvc === 2) {
		setNumberFormGroupToError('.cvc-group')
	} else if (update.status.cvc === 0) {
		setNumberFormGroupToSuccess('.cvc-group')
	} else {
		setNumberFormGroupToNormal('.cvc-group')
	}
})
$('form').on('submit', (event) => {
	event.preventDefault()
	const tappayStatus = TPDirect.card.getTappayFieldsStatus()
	console.log(tappayStatus)
	if (tappayStatus.canGetPrime === false) {
		$('#curl').html('can not get prime');
	}

	TPDirect.card.getPrime((result) => {
		if (result.status !== 0) {
			$('#curl').html('get prime error ' + result.msg);
		}
		$.ajax({
			url:  LocalUrl + "TapPay/TappayByPrime/" + result.card.prime,
		}).done(function(response) {
			$('#curl').html(JSON.stringify(response));
		});

	})
})
function setNumberFormGroupToError(selector) {
	$(selector).addClass('has-error')
	$(selector).removeClass('has-success')
}
function setNumberFormGroupToSuccess(selector) {
	$(selector).removeClass('has-error')
	$(selector).addClass('has-success')
}
function setNumberFormGroupToNormal(selector) {
	$(selector).removeClass('has-error')
	$(selector).removeClass('has-success')
}