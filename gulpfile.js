var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var elixir      = require('laravel-elixir');

elixir(function (mix) {
    mix.browserSync({
        files: ['app/**/*', 'public/**/*', 'resources/views/**/*'],
        port: 5000,
        proxy: 'ec-components'
    });
});
