<?php

use Illuminate\Http\Request;
use Illuminate\Routing\Router;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});

Route::group([
	'prefix'        => 'appfeedback',
], function (Router $router) {
	$router->post('create', 'AppfeedbackController@create');
});


/*金流API*/
Route::group([
	'prefix'        => 'SpgatewayMPG',
], function (Router $router) {
	$router->get('checkData/{MerchantOrderNo}/{Amt}', 'SpgatewayMPGController@checkData');
	$router->get('mpg/{MerchantOrderNo}/{Amt}/{ItemDesc}/{Email}', 'SpgatewayMPGController@mpg');
});


/*發票API*/
Route::group([
	'prefix'        => 'SpgatewayInvoice',
], function (Router $router) {
	$router->get('checkData/{MerchantOrderNo}/{TotalAmt}/{InvoiceNumber}', 'SpgatewayInvoiceController@checkData');

	$router->post('invoice_issue/{MerchantOrderNo}/{type}', 'SpgatewayInvoiceController@invoice_issue');

	$router->get('invoice_touch_issue/{MerchantOrderNo}/{InvoiceTransNo}/{TotalAmt}', 'SpgatewayInvoiceController@invoice_touch_issue');

	$router->get('invoice_invalid/{InvoiceNumber}/{InvalidReason}', 'SpgatewayInvoiceController@invoice_invalid');

	$router->post('allowance_issue/{MerchantOrderNo}/{InvoiceNo}', 'SpgatewayInvoiceController@allowance_issue');

	$router->post('allowance_touch_issue/{type}/{MerchantOrderNo}/{AllowanceNo}/{AllowanceAmt}', 'SpgatewayInvoiceController@allowance_touch_issue');
});


/*購物車*/
Route::group([
	'prefix'        => 'ShoppingCart',
], function (Router $router) {
	$router->post('createOrders', 'ShoppingCartController@createOrders');
	$router->get('products/{item_no?}', 'ShoppingCartController@products');
	$router->get('showOrders/{MerchantOrderNo}', 'ShoppingCartController@showOrders');
});

/*信用卡請退款*/
Route::get('SpgatewayCreditCard/close/{CloseType}/{MerchantOrderNo}/{amount}', 'SpgatewayCreditCardController@close');
Route::get('orderDaily/dailyExcelDownload', 'OrderDailyController@dailyExcelDownload');

Route::get('sliders', 'SlidersController@index');














