<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Routing\Router;

Route::get('/', 'IndexController@index');
Route::get('/Index', 'IndexController@index');

/*聊天室*/
Route::get('/chatBot', 'ChatBotController@index')->middleware('auth');
Route::get('/chatBot/fetchMessages', 'ChatBotController@fetchMessages')->middleware('auth');
Route::post('/chatBot/sendMessage', 'ChatBotController@sendMessage')->middleware('auth');

Auth::routes();
Route::get('/Loginhome', 'HomeController@index')->name('index');

/*金流API*/
Route::group([
	'prefix'        => 'SpgatewayMPG',
], function (Router $router) {
	$router->get('mpgClientBack', 'SpgatewayMPGController@mpgClientBack');
	$router->post('mpgNotify', 'SpgatewayMPGController@mpgNotify');
	$router->post('mpgFeedback', 'SpgatewayMPGController@mpgFeedback');
	$router->get('mpgFeedback', 'SpgatewayMPGController@mpgFeedback');
	$router->get('mpg/{MerchantOrderNo}/{Amt}/{ItemDesc}/{Email}', 'SpgatewayMPGController@mpg');
});


/*購物車*/


Route::group([
	'prefix'        => 'ShoppingCart',
], function (Router $router) {
	$router->post('createSession', 'ShoppingCartController@createSession');
	$router->post('updateSession', 'ShoppingCartController@updateSession');
	$router->get('getSession', 'ShoppingCartController@getSession');
	$router->get('shoppingInfo', 'ShoppingCartController@shoppingInfo');
	$router->get('shoppingOrder', 'ShoppingCartController@shoppingOrder');
});


/*商品頁*/
Route::group([
	'prefix'        => 'Products',
], function (Router $router) {
	Route::get('/', 'ProductsController@index');
	Route::get('/{id}', 'ProductsController@index');
});


/*TapPay 信用卡*/
Route::group([
	'prefix'        => 'TapPay',
], function (Router $router) {
	Route::get('/', 'TapPayController@index');
	Route::get('/TappayByPrime/{prime?}', 'TapPayController@TappayByPrime');
	Route::post('/TappayByPrime/{prime?}', 'TapPayController@TappayByPrime');
	
	Route::get('/TappayByCardToken/{prime?}', 'TapPayController@TappayByCardToken');
	Route::post('/TappayByCardToken/{prime?}', 'TapPayController@TappayByCardToken');
	Route::get('/TappayRefund/{rec_trade_id}', 'TapPayController@TappayRefund');
	Route::get('/TappayRecord', 'TapPayController@TappayRecord');
	Route::post('/TappayRecord', 'TapPayController@TappayRecord');
	Route::get('/TappayCapToday/{rec_trade_id}', 'TapPayController@TappayCapToday');
	Route::get('/TappayBindCard/{prime?}', 'TapPayController@TappayBindCard');
	Route::post('/TappayBindCard/{prime?}', 'TapPayController@TappayBindCard');
	Route::get('/TappayRemoveCard/{card_key}/{card_token}', 'TapPayController@TappayRemoveCard');
	Route::get('/TappayTradeHistory/{rec_trade_id}', 'TapPayController@TappayTradeHistory');
});


Route::get('/OrderTapPay/OrderTappayByPrime/{id}/{amount}/{prime?}/{order_number?}', 'OrderTapPayController@OrderTappayByPrime');
Route::post('/OrderTapPay/OrderTappayByPrime/{id}/{amount}/{prime?}/{order_number?}', 'OrderTapPayController@OrderTappayByPrime');











