<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.User.{id}', function ($user, $id) {
	return (int) $user->id === (int) $id;
});

Broadcast::channel('ChatBot-chatroom', function ($user) {
	return $user;
});

Broadcast::channel('ChatBot-channel', function ($user) {
	return Auth::check();
});


Broadcast::channel('online', function ($user) {
	if (auth()->check()) {
		return $user->toArray();
	}
});

Broadcast::channel('ChatBot-isTyping-channel', function ($user) {
	return Auth::check();
});


// Broadcast::channel('chat.{roomId}', function ($user, $roomId) {
//     if ($user->canJoinRoom($roomId)) {
//         return ['id' => $user->id, 'name' => $user->name];
//     }
// });