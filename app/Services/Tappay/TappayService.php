<?php
namespace App\Services\Tappay;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class TappayService
{

	private $test_prime;
	private $data;
	
	public function __construct()
	{
		$this->test_prime = 'test_3a2fb2b7e892b914a03c95dd4dd5dc7970c908df67a49527c0a648b2bc9';

	}


	public function TappayBindCard($prime, $cardholder){
		$prime      = ($prime != '') ? $prime : $this->test_prime;
		return  [
			"prime"       =>  $prime,
			"partner_key" =>  config('custom._TAPPAY_PARTNER_KEY'),
			"merchant_id" =>  config('custom._TAPPAY_MERCHANT_ID'),
			"currency"    =>  "TWD",
			"cardholder"  =>  $cardholder
		];

	}

	public function TappayRecord($filters)
	{
		$array = [
			'partner_key'  => config('custom._TAPPAY_PARTNER_KEY'),
			"records_per_page" =>  200,
			"page"    =>  1,
			"order_by" =>  [
				"attribute"     =>  'time',
				"is_descending" =>  true
			]
		];

		if(is_array($filters))
		{
			$array = array_merge($array, $filters);    
		}

		return $array;

	}

	public function card_secret($card_key, $card_token)
	{
		return [
			"partner_key" => config('custom._TAPPAY_PARTNER_KEY'),
			"card_key"    => $card_key,
			"card_token"  => $card_token,
		];
	}

	public function rec_trade_id($rec_trade_id)
	{
		return [
			"partner_key"  => config('custom._TAPPAY_PARTNER_KEY'),
			"rec_trade_id" => $rec_trade_id,
		];
	}

	public function TappayByPrime($prime = '', $data)
	{
		$prime         = ($prime != '') ? $prime : $this->test_prime;
		$TappayByPrime = [
			'card_key'     => $data['card_key'], 
			'card_token'   => $data['card_token'], 
			'prime'        => $prime,
			'partner_key'  => config('custom._TAPPAY_PARTNER_KEY'),
			'merchant_id'  => config('custom._TAPPAY_MERCHANT_ID'),
			'amount'       => $data['amount'],
			'currency'     => 'TWD',
			'order_number' => $data['order_number'], 
			'details'      => $data['details'], 
			'cardholder'   => [
				'phone_number' => $data['phone_number'], 
				'name'         => $data['name'], 
				'email'        => $data['email'], 
				'zip_code'     => $data['zip_code'], 
				'address'      => $data['address'], 
				'national_id'  => $data['national_id'], 
			],
			'instalment'            => 0,
			'delay_capture_in_days' => 0,
			'remember'              => true
		];

		return $TappayByPrime;
	}

	public function api($url, $json){
		$client     = new Client();
		$results    = $client->request('POST', config('custom._TAPPAY_URL').$url, [
			'headers' => [
				'Accept'     => 'application/json',
				'x-api-key'  => config('custom._TAPPAY_PARTNER_KEY'),
			], 
			'body'    =>  json_encode($json)

		]);
		$results_data = json_decode($results->getBody()->getContents(), true);
		return $results_data;
	}

}
