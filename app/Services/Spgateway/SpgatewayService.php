<?php
namespace App\Services\Spgateway;
use App\Services\GlobalService;
class SpgatewayService
{

	private $Hash_Key;
	private $Hash_IV;
	protected $GlobalService;

	public function __construct($Hash_Key = '', $Hash_IV = '')
	{
		$this->Hash_Key      = $Hash_Key;
		$this->Hash_IV       = $Hash_IV;
		$this->GlobalService = new GlobalService;
	}

	/**
	 * aes加密
	 * 
	 */
	public function aesEncode($data){
		$data  = http_build_query($data);

		return substr(trim(bin2hex(openssl_encrypt($this->_addpadding($data), "AES-256-CBC", $this->_getAESKey($this->Hash_Key),
			OPENSSL_RAW_DATA, $this->Hash_IV))),0, -32);
	}

	/**
	 * aes解密
	 * 
	 */
	public function aesDecrypt($return_str) {
		  return openssl_decrypt(hex2bin($return_str), "AES-256-CBC", $this->Hash_Key, OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING, $this->Hash_IV);
	}


	private function _addpadding($string, $blocksize = 32) { 
		$len     = strlen($string);
		$pad     = $blocksize - ($len % $blocksize);
		$string .= str_repeat(chr($pad), $pad);

		return $string;
	}



		/**
     * 將原始的密鑰字串，處理成 MySQL AES_ENCRYPT() 使用的密鑰格式
     * @param string $key_str 原始的密鑰字串
     * @return string
     */
		private function _getAesKey($key_str) {
			$key_len = 32; 
			$key_str_len = strlen($key_str);
			if ($key_str_len <= $key_len) {
				$pad = $key_len - $key_str_len;
				$key = $key_str . str_repeat("\0", $pad); 
			} else {
				$key = substr($key_str, 0, $key_len);
				for ($i = $key_len; $i < $key_str_len; $i++) {
					$pos = $i % $key_len;
					$key[$pos] = $key[$pos] ^ $key_str[$i];
				}
			}
			$this->key_str = $key_str;
			$this->key = $key;
			return $this->key;
		}


	}
