<?php
namespace App\Services\Spgateway;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Services\GlobalService;
use App\Services\Spgateway\SpgatewayService;

/**
* spgateway
* 智富通金流
* 
*/
class SpgatewayMPGService
{


	public $MerchantID;
	public $Hash_IV;
	public $Hash_Key;
	private $TradeInfo_array;
	private $TradeInfo;
	private $TradeSha;

	protected $GlobalService;
	protected $SpgatewayService;

	public function __construct($MerchantID = '' , $Hash_IV = '', $Hash_Key = '')
	{
		$this->MerchantID       = $MerchantID;
		$this->Hash_IV          = $Hash_IV;
		$this->Hash_Key         = $Hash_Key;
		$this->GlobalService    = new GlobalService;
		$this->SpgatewayService = new SpgatewayService($this->Hash_Key, $this->Hash_IV);
	}

	/**
	 * 檢查交易狀態
	 */
	public function checkData($url, $method, $Amt, $MerchantOrderNo){
		$client     = new Client();
		$results    = $client->request('POST', $url, [
			'form_params' => [
				'MerchantID'      => $this->MerchantID,
				'Version'         => '1.1',
				'RespondType'     => 'JSON',
				'CheckValue'      => SpgatewayMPGService::returnCheckValue($Amt, $MerchantOrderNo),
				'TimeStamp'       => time(),
				'MerchantOrderNo' => $MerchantOrderNo,
				'Amt'             => $Amt
			]
		]);

		$results_data = $results->getBody();
		return json_decode($results_data);
	}


	/**
	 * 檢查交易狀態
	 * 回復CheckValue
	 */
	private function returnCheckValue($Amt, $MerchantOrderNo){
		$check_code_str = 'IV=' .$this->Hash_IV. '&Amt=' .$Amt. '&MerchantID=' .$this->MerchantID. '&MerchantOrderNo=' .$MerchantOrderNo. '&Key=' .$this->Hash_Key;
		return strtoupper(hash('sha256', $check_code_str));
	}



	/**
	 * MPG金流串接
	 * 
	 */
	public function mpg($MerchantOrderNo, $Amt, $ItemDesc = '', $email){
		$this->tradeInfoAesEncode($MerchantOrderNo, $Amt, $ItemDesc, $email);
		$this->returnTradeSha();
		return array(
			'MerchantID' => $this->MerchantID,
			'TradeInfo'  => $this->TradeInfo,
			'TradeSha'   => $this->TradeSha,
			'Version'    => '1.4'
		);
	}



	/**
	 * MPG串接支付完成資料
	 * 
	 */
	public function mpgFeedback($request){		
		$data    = $this->GlobalService->deleteNonUtf8($this->SpgatewayService->aesDecrypt($request->input('TradeInfo')));
		$results = json_decode($data, true);
		return $results;
	}




	/**
	 * EmailModify 付款人電子信箱是否開放修改
	 * 1.設定於 MPG 頁面，付款人電子信箱欄位 是否開放讓付款人修改。
	 * 1=可修改
	 * 0=不可修改 
	 * 2.當未提供此參數時，將預設為可修改。
	 *
	 *
	 * LoginType 智付通會員
	 * 1 = 須要登入智付通會員 
	 * 0 = 不須登入智付通會員
	 *
	 * CREDIT 信用卡 一次付清啟用
	 * 1.設定是否啟用信用卡一次付清支付方式。 
	 * 1 =啟用
	 * 0 或者未有此參數=不啟用
	 *
	 * InstFlag 信用卡 分期付款啟用
	 *  1.此欄位值=1 時，即代表開啟所有分期期別，且不可帶入其他期別參數。
	 *  2.此欄位值為下列數值時，即代表開啟該分期期別。
	 *  3=分 3 期功能
	 *  6=分 6 期功能
	 *  12=分 12 期功能
	 *  18=分 18 期功能
	 *  24=分 24 期功能
	 *  30=分 30 期功能
	 *  3.同時開啟多期別時，將此參數用”，”(半形)分隔，例如:3,6,12，代表開啟 分 3、6、12 期的功能。
	 *  4. 此欄位值=0或無值時，即代表不開啟分期。
	 *
	 *	CreditRed 信用卡紅利啟用
	 * 	1.設定是否啟用信用卡紅利支付方式。 
	 * 	1 =啟用
	 *  0 或者未有此參數=不啟用
	 *
	 *	UNIONPAY 信用卡銀聯卡啟用
	 *	1.設定是否啟用銀聯卡支付方式。 
	 *	1=啟用
	 *	0 或者未有此參數=不啟用
	 * 
	 *
	 *	WEBATM WEBATM啟用
	 *	1.設定是否啟用 WEBATM 支付方式。
	 *	1=啟用
	 *	0 或者未有此參數，即代表不開啟。
	 *
	 *	VACC ATM 轉帳啟用
	 *  1.設定是否啟用 ATM 轉帳支付方式。
	 *  1 = 啟用
	 *  0 或者未有此參數=不啟用
	 *
	 *
	 *  CVS 超商代碼繳費啟用
	 *  1.設定是否啟用超商代碼繳費支付方式 1 = 啟用
	 *  0 或者未有此參數，即代表不開啟。
	 *  2.當該筆訂單金額小於 30 元或超過 2 萬元 時，即使此參數設定為啟用，MPG 付款頁 面仍不會顯示此支付方式選項。
	 *
	 *  BARCODE 超商條碼繳費啟用
	 *  1.設定是否啟用超商條碼繳費支付方式 1 = 啟用
	 *  0 或者未有此參數，即代表不開啟。
	 *  2.當該筆訂單金額小於 20 元或超過 4 萬元 時，即使此參數設定為啟用，MPG 付款頁 面仍不會顯示此支付方式選項。
	 *
	 * 
	 * 
	 */

	public function returnTradeInfoArray($MerchantOrderNo, $Amt, $ItemDesc, $email) {
		return array(
			'MerchantID'      => $this->MerchantID,
			'RespondType'     => 'JSON',
			'TimeStamp'       => time(),
			'Version'         => '1.4',
			'LangType'        => 'zh-tw',
			'MerchantOrderNo' => $MerchantOrderNo,
			'Amt'             => $Amt,
			'ItemDesc'        => $ItemDesc,
			'ReturnURL'       => url('/SpgatewayMPG/mpgFeedback'),
			'NotifyURL'       => url('/SpgatewayMPG/mpgNotify'),
			'ClientBackURL'   => url('/SpgatewayMPG/mpgClientBack'),
			'CustomerURL'     => url('/SpgatewayMPG/mpgFeedback'),
			'Email'           => $email,
			'EmailModify'     => 0,
			'LoginType'       => 0,
			'OrderComment'    => '商店備註',	
			'CREDIT'          => 1,
			'InstFlag'        => 1,
			'CreditRed'       => 0,
			'UNIONPAY'        => 0,
			'WEBATM'          => 1,
			'VACC'            => 1,
			'CVS'             => 1,
			'BARCODE'         => 1,
			'TokenTerm'       => 'user_id',
		);
	}


	/**
	 * return TradeSha
	 * 
	 */
	private function returnTradeSha() { 
		$TradeInfo      = 'HashKey='. $this->Hash_Key. '&' .$this->TradeInfo. '&HashIV=' .$this->Hash_IV;
		$this->TradeSha = strtoupper(hash("sha256", $TradeInfo));
	}


	/**
	 * return tradeInfo
	 * 
	 */
	private function tradeInfoAesEncode($MerchantOrderNo, $Amt, $ItemDesc, $email) { 
		$TradeInfoArray  = $this->returnTradeInfoArray($MerchantOrderNo, $Amt, $ItemDesc, $email);
		$this->TradeInfo = $this->SpgatewayService->aesEncode($TradeInfoArray);
	}

}
