<?php
namespace App\Services\Spgateway;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Services\Spgateway\SpgatewayService;

/**
* spgateway
* 智富通信用卡-請退款
* 
*/
class SpgatewayCreditCardService
{

	/**
	 * 測試環境的key
	 * 
	 */
	private $MerchantID_;
	private $Hash_IV;
	private $Hash_Key;
	private $PostData_array;
	private $PostData_;
	private $url;
	private $method;

	protected $GlobalService;
	protected $SpgatewayService;

	public function __construct($MerchantID_ = '' , $Hash_IV = '', $Hash_Key = '')
	{
		$this->MerchantID_      = $MerchantID_;
		$this->Hash_IV          = $Hash_IV;
		$this->Hash_Key         = $Hash_Key;
		$this->SpgatewayService = new SpgatewayService($this->Hash_Key, $this->Hash_IV);
	}


	/**
	 * 信用卡請退款
	 *
	 * MerchantOrderNo
	 * 訂單編號
	 *
	 * amount
	 * 請退款金額
	 *
	 * CloseType
	 * 1請款 2退款
	 * 
	 */
	public function close($url, $method, $CloseType, $MerchantOrderNo, $amount){
		$this->url    = $url;
		$this->method = $method; 
		$this->PostData_array = array(
			"RespondType"     => "JSON",
			"Version"         => "1.0",
			"Amt"             => $amount,
			"MerchantOrderNo" => $MerchantOrderNo,
			"TimeStamp"       => strval(time()),
			"IndexType"       => 1,
			"TradeNo"         => "",
			"CloseType"       => $CloseType 
		);



		return $this->PostDataAesEncode();
	}

	/**
	 * API接口
	 * 
	 */
	private function urlPost(){
		$client     = new Client();
		$results    = $client->request($this->method, $this->url, [
			'form_params' => [
				'MerchantID_' => $this->MerchantID_,
				'PostData_'   => $this->PostData_,
			]
		]);


		return $results->getBody();
	}


	/**
	 * return PostData
	 * 
	 */
	private function PostDataAesEncode() { 
		$this->PostData_ = $this->SpgatewayService->aesEncode($this->PostData_array);
		$json            = $this->urlPost();
		return json_decode($json);
	}



}
