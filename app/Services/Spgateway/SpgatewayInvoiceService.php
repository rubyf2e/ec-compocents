<?php
namespace App\Services\Spgateway;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Services\Spgateway\SpgatewayService;

/**
* spgateway
* 智富通電子發票
* 
*/
class SpgatewayInvoiceService
{

	/**
	 * 測試環境的key
	 * 
	 */
	private $MerchantID_;
	private $Hash_IV;
	private $Hash_Key;
	private $PostData_array;
	private $PostData_;
	private $url;
	private $method;

	protected $GlobalService;
	protected $SpgatewayService;

	public function __construct($MerchantID_ = '' , $Hash_IV = '', $Hash_Key = '')
	{
		$this->MerchantID_      = $MerchantID_;
		$this->Hash_IV          = $Hash_IV;
		$this->Hash_Key         = $Hash_Key;
		$this->SpgatewayService = new SpgatewayService($this->Hash_Key, $this->Hash_IV);
	}


	/**
	 * 查詢發票
	 *
	 * MerchantOrderNo
	 * 訂單編號
	 *
	 * TotalAmt
	 * 發票金額
	 *
	 * InvoiceNumber
	 * 發票號碼
	 *
	 * RandomNum
	 * 發票防偽隨機碼
	 * 
	 */
	public function checkData($url, $method, $MerchantOrderNo, $TotalAmt, $InvoiceNumber, $RandomNum){
		$this->url    = $url;
		$this->method = $method; 
		$this->PostData_array = array(
			'RespondType'     => 'JSON', 
			'Version'         => '1.1', 
			'TimeStamp'       => time(),
			'SearchType'      => 1,
			'MerchantOrderNo' => $MerchantOrderNo,
			'TotalAmt'        => $TotalAmt,
			'InvoiceNumber'   => $InvoiceNumber,
			'RandomNum'       => $RandomNum,
		);

		return $this->PostDataAesEncode();
	}


	/**
	 * 開立發票參數設定
	 * SearchType
	 * 0=使用發票號碼及隨機碼查詢。 1=使用訂單編號及發票金額查詢。
	 *(若沒帶此參數，則預設為 0 )
	 *
	 * Status
	 * 1=即時開立發票
	 * 0=等待觸發開立發票
	 * 3=預約自動開立發票(須指定預計開立日期)
	 *
	 * 
	 * TotalAmt
	 * 開立發票的總金額
	 *
	 * InvoiceNumber
	 * 此次查詢的發票號碼
	 *
	 * RandomNum
	 * 開立發票時，回傳的 4 碼發票防偽隨機碼
	 *
	 * TransNum
	 * 智付寶平台交易序號
	 * 
	 */
	public function invoice_issue($url, $method, $MerchantOrderNo, $type, $data){
		$this->url    = $url;
		$this->method = $method; 
		$this->PostData_array = array(
			'RespondType'     => 'JSON', 
			'Version'         => '1.4', 
			'TimeStamp'       => time(),
			'TransNum'        => '',
			'MerchantOrderNo' => $MerchantOrderNo,
			'Status'          => $type,
			'Category'        => 'B2C',
			'BuyerName'       => $data['BuyerName'],
			'BuyerAddress'    => $data['BuyerAddress'],
			'BuyerEmail'      => $data['BuyerEmail'],
			'BuyerPhone'      => $data['BuyerPhone'],
			'PrintFlag'       => 'N',
			'TaxType'         => '1',
			'TaxRate'         => '5',
			'CarrierType'     => '2',

			/*銷售額 未稅*/
			'Amt'             => $data['Amt'], 

			/*稅額*/
			'TaxAmt'          => $data['TaxAmt'], 

			/*總計 = 銷售額+稅額*/
			'TotalAmt'        => $data['TotalAmt'], 

			'ItemName'        => $data['ItemName'], 
			'ItemCount'       => $data['ItemCount'], 
			'ItemUnit'        => $data['ItemUnit'], 

			/*單價 = 銷售額+稅額*/
			'ItemPrice'       => $data['ItemPrice'], 

			/*金額 = 銷售額+稅額*/
			'ItemAmt'         => $data['ItemAmt'], 

			'Comment'         => $data['Comment'], 
			'NotifyEmail'     => '1' 
		);


		/**
		 * 二聯索取發票
		 */
		if ($data['type'] == 2) {
			$this->PostData_array['PrintFlag']   = 'Y';
			$this->PostData_array['CarrierType'] = '';
			$this->PostData_array['CarrierNum']  = '';
		}

		/**
		 * 三聯發票
		 */
		if ($data['type'] == 3) {
			$this->PostData_array['Category']    = 'B2B';
			$this->PostData_array['BuyerUBN']    = $data['BuyerUBN'];
			$this->PostData_array['PrintFlag']   = 'Y';
			$this->PostData_array['CarrierType'] = '';
			$this->PostData_array['CarrierNum']  = '';
		}

		return $this->PostDataAesEncode();
	}

	/**
	* 觸發已開立發票
	* 
	*/
	public function invoice_touch_issue($url, $method, $MerchantOrderNo, $InvoiceTransNo, $TotalAmt){
		$this->url    = $url;
		$this->method = $method; 
		$this->PostData_array = array(
			'RespondType'     => 'JSON', 
			'Version'         => '1.0', 
			'TimeStamp'       => time(),
			'TransNum'        => '',
			'InvoiceTransNo'  => $InvoiceTransNo,
			'MerchantOrderNo' => $MerchantOrderNo,
			
			/*總計 = 銷售額+稅額*/
			'TotalAmt'        => $TotalAmt
		);

		return $this->PostDataAesEncode();
	}


	/**
	* 開立折讓
	* 
	* InvoiceNo
	* 發票號碼
	*/
	public function allowance_issue($url, $method, $MerchantOrderNo, $InvoiceNo, $data){
		$this->url            = $url;
		$this->method         = $method; 
		$this->PostData_array = array(
			'RespondType'     => 'JSON', 
			'Version'         => '1.3', 
			'TimeStamp'       => time(),
			'InvoiceNo'       => $InvoiceNo,
			'MerchantOrderNo' => $MerchantOrderNo,

			'ItemName'        => $data['ItemName'], 
			'ItemCount'       => $data['ItemCount'], 
			'ItemUnit'        => $data['ItemUnit'], 

			/*單價 = 銷售額+稅額*/
			'ItemPrice'       => $data['ItemPrice'], 

			/*金額 = 銷售額+稅額*/
			'ItemAmt'         => $data['ItemAmt'], 

			'ItemTaxAmt'      => $data['ItemTaxAmt'],

			/*總計 = 銷售額+稅額*/
			'TotalAmt'        => $data['TotalAmt'], 
			'Status'          => '0',
		);

		return $this->PostDataAesEncode();
	}


	/**
	* 觸發確認折讓
	* allowance_touch_issue
	* C = 確認折讓。
	* D = 取消折讓。
	* 
	*/
	public function allowance_touch_issue($url, $method, $type, $MerchantOrderNo, $AllowanceNo, $TotalAmt){
		$this->url            = $url;
		$this->method         = $method; 
		$this->PostData_array = array(
			'RespondType'     => 'JSON', 
			'Version'         => '1.0', 
			'TimeStamp'       => time(),
			'AllowanceStatus' => $type,
			'MerchantOrderNo' => $MerchantOrderNo,
			'AllowanceNo'     => $AllowanceNo,
			'TotalAmt'        => $TotalAmt
		);

		return $this->PostDataAesEncode();
	}


	/**
	* 作廢發票
	* InvoiceNumber
	* 發票號碼
	*/
	public function invoice_invalid($url, $method, $InvoiceNumber, $InvalidReason){
		$this->url            = $url;
		$this->method         = $method; 
		$this->PostData_array = array(
			'RespondType'     => 'JSON', 
			'Version'         => '1.0', 
			'TimeStamp'       => time(),
			'InvoiceNumber'   => $InvoiceNumber,
			'InvalidReason'   => $InvalidReason,
		);

		return $this->PostDataAesEncode();
	}


	/**
	 * API接口
	 * 
	 */
	private function urlPost(){
		$client     = new Client();
		$results    = $client->request($this->method, $this->url, [
			'form_params' => [
				'MerchantID_' => $this->MerchantID_,
				'PostData_'   => $this->PostData_,
			]
		]);

		return $results->getBody();
	}


	/**
	 * return PostData
	 * 
	 */
	private function PostDataAesEncode() { 
		$this->PostData_ = $this->SpgatewayService->aesEncode($this->PostData_array);
		$json            = $this->urlPost();
		return json_decode($json, true, 3);
	}

}
