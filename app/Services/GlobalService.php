<?php
namespace App\Services;

class GlobalService
{
	/** 
	 *  deleteNonUtf8
	 * 移除非utf8字元
	 *  
	 */
	public function deleteNonUtf8($data){
		$non_utf8 = preg_replace('%(  
			[\x09\x0A\x0D\x20-\x7E]  
			| [\xC2-\xDF][\x80-\xBF]  
			| \xE0[\xA0-\xBF][\x80-\xBF]  
			| [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  
			| \xED[\x80-\x9F][\x80-\xBF]  
			| \xF0[\x90-\xBF][\x80-\xBF]{2}  
			| [\xF1-\xF3][\x80-\xBF]{3}  
			| \xF4[\x80-\x8F][\x80-\xBF]{2}  
		)%xs', '', $data); 
		return str_replace($non_utf8, '', $data); 
	}

}
