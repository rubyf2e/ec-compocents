<?php
namespace App\Services;

class RequestService
{
	private $target;
	private $request;

	public function __construct($data = '', $request = '')
	{
		$this->data    = $data;
		$this->request = $request;
	}

	public function InputValueDefaultSet($column){
		$value = $this->request->input($column);
		return 	($value != null) ? $value : $this->data[$column];
	}

	public function returnNotNull($value){
		return 	($value != null) ? $value : '';
	}

}
