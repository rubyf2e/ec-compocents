<?php
namespace App\Services;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class ApiHashService
{
	private $apiHashGet_data;
	private $header_token;
	private $header_client;
	private $hashing_algorithm;	

	public function __construct($header_token = '', $header_client = '', $hashing_algorithm = '')
	{
		$this->header_token      = $header_token;
		$this->header_client     = $header_client;
		$this->hashing_algorithm = $hashing_algorithm;
	}

	/** 
	 *  responseAddXHeader
	 *  將API加上自定義X-Seed等 header
	 *  
	 */
	public function responseAddXHeader($results, $X_Seed, $X_User, $X_Sign){
		return response($results, 200)
		->header('Content-Type', "application/json; charset=utf-8")
		->header('X-Service-Client', $this->header_client)
		->header('X-Service-Token', $this->header_token)
		->header('X-Seed', $X_Seed)
		->header('X-User', $X_User)
		->header('X-Sign', $X_Sign);
	}


	/** 
	 *  apiHeadersGet
	 *  帶入自定義X-Seed等 header 向 $url 取得資料
	 *  
	 */
	public function apiHeadersGet($method, $url, $X_Seed, $X_User, $X_Sign){
		$client            = new Client();
		$results           = $client->request($method, $url,[
			'headers'        => [
				'X-Service-Token'  => $this->header_token,
				'X-Service-Client' => $this->header_client,
				'X-Seed'           => $X_Seed,
				'X-User'           => $X_User,
				'X-Sign'           => $X_Sign
			]
		]);

		$results_data  = $results->getBody();
		return response()->json(json_decode($results_data) ,  200);
	}


	/** 
	 *  apiHashGetData
	 *  驗證$hashing_algorithm形態的API並輸出資料
	 *  
	 */
	
	public function apiHashGetData($method, $host, $path){
		$results = ApiHashService::apiHashGet($method, $host, $path); 
		return response()->json(json_decode($this->apiHashGet_data) ,  200); 
	}


	/** 
	 *  apiHashGet
	 *  驗證$hashing_algorithm形態的API並輸出資料status
	 *  
	 */
	public function apiHashGet($method, $host, $path){
		$client                = new Client();
		$data                  = $client->get($host.$path);
		$this->apiHashGet_data = $data->getBody();
		$results_seed          = $data->getHeader('X-Seed')[0];
		$results_client        = $data->getHeader('X-Service-Client');
		$results_sign          = $data->getHeader('X-Sign')[0];
		$target_path           = $method . "\n" . $path . "\n" . $results_seed;	
		$base64_hash           = ApiHashService::_generateHash($target_path);
		$results               = ($base64_hash === $results_sign) ? ['status' => true] : ['status' => false];

		return response()->json($results,  200);
	}


	/** 
	 *  apiHashPost
	 *  驗證X_Sign 後回覆status
	 *  
	 */
	
	public function apiHashPost($method, $path, $X_Seed, $X_Sign){
		$target_path = $method . "\n/" . $path . "\n" . $X_Seed;	
		$base64_hash = ApiHashService::_generateHash($target_path);
		$results     = ($base64_hash === $X_Sign) ?  true : false;
		return $results;
	}


	/** 
	 *  _generateHash
	 *  加密程式 return 計算後的X-Sign
	 *  
	 */
	private function _generateHash($target_path){
		$hash   = hash_hmac($this->hashing_algorithm, $target_path, $this->header_token, true);
		$X_Sign = base64_encode($hash);
		return 	$X_Sign;
	}

}
