<?php

namespace App\Model;

use DB;
use Carbon\Carbon;
use App\Services\RequestService;
use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
	protected $table   = 'member';
	public $timestamps = false;

	public function insert($data){
		DB::table('member')->insert($data);
		return true;
	}

}
