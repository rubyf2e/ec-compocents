<?php

namespace App\Model;

use DB;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
	protected $table = 'products';

	public function products($item_no = ''){

		if($item_no){
			$data = $this::where('item_no', $item_no)->get();
		}
		else
		{
			$data = $this::all();	
		}

		$results = array();

		foreach ($data as $key => $result) {
			$results[$result->item_no]['id']       = $result->item_no;
			$results[$result->item_no]['name']     = $result->name;
			$results[$result->item_no]['price']    = $result->price;
			$results[$result->item_no]['tax']      = 0;
			$results[$result->item_no]['subtotal'] = $result->price;
		}

		return $results;
	}
}
