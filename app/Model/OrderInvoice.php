<?php

namespace App\Model;

use DB;
use Carbon\Carbon;
use App\Services\RequestService;
use Illuminate\Database\Eloquent\Model;

class OrderInvoice extends Model
{

  public function selectOrderstatus($order_no){
    $result    = DB::table('orders')
    ->selectRaw('order_status')
    ->where('order_no', '=', $order_no)
    ->get();

    return $result;
  }

  public function updateOrderstatus($order_no){
    $result = DB::table('orders')
    ->where('order_no', '=', $order_no)
    ->update(['order_status' => 4]);

    return true;
  }


  public function insert_invoice_issue_histories($id, $web_info){
    DB::table('payment_sp_gateway_invoice_issue_histories')->insert([
      [
        'order_id'          => $id,
        'status'            => $web_info['Status'],
        'message'           => $web_info['Message'],
        'invoice_trans_no'  => $web_info['Result']['InvoiceTransNo'],
        'merchant_order_no' => $web_info['Result']['MerchantOrderNo'],
        'total_amt'         => $web_info['Result']['TotalAmt'],
        'invoice_no'        => $web_info['Result']['InvoiceNumber'],
        'random_num'        => $web_info['Result']['RandomNum'],
        'create_time'       => Carbon::now(),
        'check_code'        => $web_info['Result']['CheckCode'],
        'bar_code'          => $web_info['Result']['BarCode'],
        'qr_code_left'      => $web_info['Result']['QRcodeL'],
        'created_at'        => Carbon::now(),
        'updated_at'        => Carbon::now(),
      ]
    ]);
  }


  public function selectOrders($order_no){
    $orders = [];
    $result = DB::table('orders')
    ->selectRaw('id, order_no, created_at, email, orderer, reception_phone, address_city, address_township, address, receipt_type, receipt_to_tax_id, receipt_title_to, (amount+shipping_cost) as amount, shipping_cost, user_id, redeem_points, request_paper_invoice')
    ->where('order_no', '=', $order_no)
    ->whereIn('order_status', [1,2,4,5])
    ->orderBy('id', 'DESC')
    ->get();

    foreach ($result as $row) {
      $orders[$row->order_no] = [
        'id'                    => $row->id,
        'order_no_new'          => $row->order_no,
        'created_at'            => $row->created_at,
        'email'                 => $row->email,
        'orderer'               => $row->orderer,
        'reception_phone'       => $row->reception_phone,
        'address_city'          => $row->address_city,
        'address_township'      => $row->address_township,
        'address'               => $row->address,
        'receipt_type'          => $row->receipt_type,
        'receipt_to_tax_id'     => $row->receipt_to_tax_id,
        'receipt_title_to'      => $row->receipt_title_to,
        'amount'                => $row->amount,
        'shipping_cost'         => $row->shipping_cost,
        'user_id'               => $row->user_id,
        'redeem_points'         => $row->redeem_points,
        'request_paper_invoice' => $row->request_paper_invoice,
      ];
    }

    return $orders;
  }

  public function selectOrdersItems($orders){
    $order_items = [];
    foreach ($orders as $order_no => $order) {
      $order_items[$order_no] = [];
      $results = DB::table('order_items AS o')
      ->join('products AS p', function($join)
      {
        $join->on('o.sku', '=', 'p.sku');

      })->select('o.sku', 'o.qty', 'o.redeemd', 'o.selling_price', 'p.name')
      ->where('o.order_id', '=', $order['id'])
      ->where('o.item_status', '=', 0)
      ->orderBy('o.id', 'DESC')
      ->get();

      foreach ($results as $key => $row) {
        $itemPrice = $row->selling_price - $row->redeemd;
        $itemAmt   = $itemPrice * $row->qty;
        array_push(
          $order_items[$order_no],
          [
            'ItemCount' => $row->qty,
            'ItemPrice' => $itemPrice,
            'ItemAmt'   => $itemAmt,
            'ItemName'  => $row->name
          ]
        ); 
      }


      /*如果有運費就要強制補一個運費進去*/
      if ($orders[$order_no]['shipping_cost'] > 0) {
        $itemPrice = 120;
        $itemAmt   = $itemPrice * 1;

        array_push(
          $order_items[$order_no],
          [
            'ItemCount' => '1',
            'ItemPrice' => $itemPrice,
            'ItemAmt'   => $itemAmt,
            'ItemName'  => '運費'
          ]
        );
      }

    }

    return $order_items;
  }


  public function selectCard($orders){
    $card_no_four = [];
    foreach ($orders as $order_no => $order) {
      $result = DB::table('payment_sp_gateway_histories')
      ->selectRaw('card_no_four')
      ->where('merchant_order_no', '=', $order_no)
      ->where('status', '=',  'SUCCESS')
      ->get();

      if(count($result) !== 0){
        $card_no_four[$order_no] = $result[0]->card_no_four;
      }
      else
      {
        $card_no_four[$order_no] = false;
      }
    }

    return $card_no_four;
  }

  public function checkInvoice($order_no, $id){
   $result = DB::table('payment_sp_gateway_invoice_issue_histories')
   ->selectRaw('count(*) AS count')
   ->where('order_id', '=', $id)
   ->get();

   if($result[0]->count !== 0){
     return true;
   }
   else
   {
    return false;
  }
  
}







}
