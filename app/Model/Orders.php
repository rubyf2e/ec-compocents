<?php

namespace App\Model;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\Services\RequestService;
use DateTime;

class Orders extends Model
{
	protected $table   = 'spgateway_orders';
	protected $GlobalService;
	private $OrderPrefix;
	private $standardDate;
	private $orderNum;
	private $now;


	public function __construct()
	{
		$this->now           = new DateTime ();
		$this->OrderPrefix   = config('custom.OrderPrefix');
		$this->standardDate  = $this->now->format('Ymd');
		$this->orderNum      = $this->OrderPrefix .$this->standardDate;
	}

	/**
	 * 訂定訂單編號
	 * 
	 */
	public function returnOrdersId(){
		$orders_id  = $this->orderNum. '0001';
		$checkLast  = Orders::where('orders_id', 'like', $this->orderNum .'%')->orderBy('id', 'DESC')->take(1)->get();

		if(count($checkLast) === 0)
		{
			return $orders_id;
		}
		else
		{
			$orders_num        = substr($checkLast[0]->orders_id, -4);
			$orders_num        = (int)$orders_num;
			$orders_num++;
			$orders_num        = substr('000' .$orders_num, -4);
			$orders_id         = $this->orderNum. $orders_num;
			return $orders_id;
		}

	}

	/**
	 * 存入訂單
	 * 
	 */
	public function add($orders_id, $amount){
		$Orders            = new Orders;
		$Orders->orders_id = $orders_id;
		$Orders->amount    = $amount;
		$Orders->save();
	}

	public function showOrders($MerchantOrderNo){
		$checkLast  = Orders::where('orders_id', '=', $MerchantOrderNo)->orderBy('id', 'DESC')->take(1)->get();
		return $checkLast[0];
	}

	public function createOrders($data){
		$Orders                      = new Orders;
		$RequestService              = new RequestService();
		$MerchantOrderNo             = $Orders->returnOrdersId();
		$Orders->orders_id           = $MerchantOrderNo;
		$Orders->firstName           = $RequestService->returnNotNull($data['firstName']);
		$Orders->lastName            = $RequestService->returnNotNull($data['lastName']);
		$Orders->name                = $RequestService->returnNotNull($data['name']);
		$Orders->recipient           = $RequestService->returnNotNull($data['recipient']);
		$Orders->email               = $RequestService->returnNotNull($data['email']);
		$Orders->tel                 = $RequestService->returnNotNull($data['tel']);
		$Orders->shipping            = $data['shipping'];
		$Orders->address             = $RequestService->returnNotNull($data['address']);
		$Orders->storeSelect         = $data['storeSelect'];
		$Orders->receipt             = $data['receipt'];
		$Orders->receiptTitle        = $RequestService->returnNotNull($data['receiptTitle']);
		$Orders->receiptTax          = $RequestService->returnNotNull($data['receiptTax']);
		$Orders->requestPaperInvoice = $data['requestPaperInvoice'];
		$Orders->comment             = $RequestService->returnNotNull($data['comment']);
		$Orders->cartContent         = $RequestService->returnNotNull($data['cartContent']);
		$Orders->subtotal            = $data['subtotal'];
		$Orders->save();

		return $MerchantOrderNo;

	}


}
