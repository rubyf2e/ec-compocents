<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ChatBot extends Model
{
	protected $table    = 'messages';
	protected $fillable = ['message', 'user_email', 'user_name'];

    /**
    * A message belong to a user
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function user()
    {
    	return $this->belongsTo(User::class);
    }

 }
