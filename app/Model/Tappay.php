<?php

namespace App\Model;

use DB;
use Carbon\Carbon;
use App\Services\RequestService;
use Illuminate\Database\Eloquent\Model;

class Tappay extends Model
{

	public function TappayByPrimeAddLog($data){

		DB::table('tappay_auth_log')->insert($data);
		return true;
	}

	public function TappayByPrimeUpdateLog($data){
		DB::table('tappay_auth_log')
		->where('order_no', '=', $data['order_no'])
		->update($data);
		return true;
	}



}
