<?php

namespace App\Admin\Controllers;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Support\Facades\Request;
use App\Model\Orders;


class OrdersController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('訂單列表');
            $content->body($this->grid());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {

        return Admin::grid(Orders::class, function (Grid $grid) {

            $grid->filter(function ($filter) {
                $filter->between('created_at', '創立日期')->datetime();
                $filter->between('orders_id', '訂單編號')->datetime();
                $filter->between('recipient', '收件人')->datetime();
            });


            $grid->rows(function (Grid\Row $row) {
              $this->orders_id   = $row->orders_id;
              $this->shipping    = $row->shipping;
              $this->address     = $row->address;
              $this->storeSelect = $row->storeSelect;
          });

            $grid->disableCreation();
            $grid->disableActions();

            $grid->model()->orderBy('id', 'DESC');
            $grid->paginate(20);
            $grid->id('ID')->sortable();




            $grid->column('orders_id', '訂單編號');
            $grid->column('name', '姓名');

            $grid->column('recipient', '收件人');
            $grid->column('email', 'email');
            $grid->column('tel', '電話');
            $grid->column('配送方式')->display(function ($shipping) {

                if($this->shipping === 1){
                 return '宅配配送<br>地址：'. $this->address;
             }
             else{
                 return '門市取貨<br>門市：'. $this->storeSelect;
             }

             return ($this->shipping === 1) ? '配送' : '門市取貨';
         });

            $grid->cartContent('訂購商品')->display(function ($cartContent) {
                $cartContent = json_decode($cartContent, true);
                $template    = '';

                if(count($cartContent) > 0){
                   $template    = '<table class="table table-hover">
                   <tbody>
                   <tr>
                   <th>產品編號</th>
                   <th>產品名稱</th>
                   <th>數量</th>
                   <th>未稅價</th>
                   <th>稅額</th>
                   <th>總額</th>
                   </tr>
                   <tr>';

                   foreach ($cartContent as $id => $item) {
                    $template    .= '<th>' .$id. '</th>';
                    $template    .= '<th>' .$item['originalProductName']. '</th>'; 
                    $template    .= '<th>' .$item['value']. '</th>'; 
                    $template    .= '<th>' .$item['price']. '</th>'; 
                    $template    .= '<th>' .$item['tax']. '</th>'; 
                    $template    .= '<th>' .$item['total']. '</th>'; 
                    $template    .= '</tr>';
                }

                $template    .= '</tbody></table>';  
            }

            return $template;

        }); 
            $grid->column('subTotal', '總額');
            $grid->column('comment', '訂單備註');
            $grid->column('發票狀態')->invoiceState(function () {
                $orders = Orders::showOrders($this->orders_id);
                return [
                    $orders->orders_id,
                    $orders->subTotal,
                    $orders->receipt,
                    $orders->receiptTitle,
                    $orders->receiptTax,
                    $orders->requestPaperInvoice,
                ];

            }, url('/api/SpgatewayInvoice/checkData'));
            
            $grid->column('created_at', '創立日期');
            $grid->column('updated_at', '更新日期');

        });
    }


}
