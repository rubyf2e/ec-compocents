<?php

namespace App\Admin\Controllers;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Support\Facades\Request;
use App\Model\Products;


class ProductsController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('產品列表');
            $content->body($this->grid());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {

        return Admin::grid(Products::class, function (Grid $grid) {
            $grid->rows(function (Grid\Row $row) {
              $this->name    = $row->name;
              $this->item_no = $row->item_no;

          });

            $grid->column('id', 'ID');
            $grid->column('item_no', '產品編號');
            $grid->column('產品名稱')->display(function ($shipping) {
                return $this->name;
            });
            $grid->src('圖片')->image();
            $grid->column('price', '未稅價');
            $grid->column('tax', '稅額');
            $grid->column('subtotal', '總額');

            $grid->column('created_at', '創立日期');
            $grid->column('updated_at', '更新日期');

        });
    }


     /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
     public function edit($id)
     {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('產品列表');
            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {

        return Admin::content(function (Content $content) {
            $content->header('產品列表');
            $content->body($this->form());
        });
    }


     /**
     * Make a form builder.
     *
     * @return Form
     */
     protected function form()
     {
        return Admin::form(Products::class, function (Form $form) {

            $form->rows(function (Grid\Row $row) {
              $this->price   = $row->price;
              
          });

            $form->display('id', 'ID');
            $form->text('item_no', '產品編號');
            $form->text('name', '產品名稱');
            $form->image('src', '產品圖片');
            $form->price('price', '未稅價');
            $form->tax('tax', '稅額');
            $form->subtotal('subtotal', '總額');

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }


}
