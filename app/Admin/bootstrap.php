<?php
use Encore\Admin\Form;
use Encore\Admin\Grid\Column;
use App\Admin\Extensions\InvoiceState;
use App\Admin\Extensions\Tax;
use App\Admin\Extensions\SubTotal;
use App\Admin\Extensions\Price;


Column::extend('invoiceState', InvoiceState::class);
Form::extend('price', Price::class);
Form::extend('tax', Tax::class);
Form::extend('subtotal', SubTotal::class);
