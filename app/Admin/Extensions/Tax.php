<?php

namespace App\Admin\Extensions;

use Encore\Admin\Form\Field;
use Encore\Admin\Form\Field\PlainInput;

class Tax extends Field
{
    use PlainInput;

    /**
     * Render this filed.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function render()
    {
        $this->initPlainInput();

$this->script = <<<EOT

var total = $('#total').val();
var tax   = total - (total / 1.05).toFixed(2);
    tax   = tax.toFixed(2);
$('{$this->getElementClassSelector()}').val(tax);

EOT;

        $this->prepend('')
            ->defaultAttribute('type', 'text')
            ->defaultAttribute('id', $this->id)
            ->defaultAttribute('readonly', 'readonly')
            ->defaultAttribute('name', $this->elementName ?: $this->formatName($this->column))
            ->defaultAttribute('value', old($this->column, $this->value()))
            ->defaultAttribute('class', 'form-control '.$this->getElementClassString())
            ->defaultAttribute('placeholder', '0.00')
            ->defaultAttribute('style', 'width: 100px');

        return parent::render()->with([
            'prepend' => $this->prepend,
            'append'  => $this->append,
        ]);


    }
}
