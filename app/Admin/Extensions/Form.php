<?php

namespace App\Admin\Extensions;
use Encore\Admin\Widgets\Form as WidgetsForm;

class Form extends WidgetsForm
{
    /**
     * Render the form.
     *
     * @return string
     */
    public function render()
    {
        return view('admin.form1', $this->getVariables())->render();
    }

}
