<?php

namespace App\Admin\Extensions;

use Encore\Admin\Admin;
use Encore\Admin\Grid\Displayers\AbstractDisplayer;

class InvoiceState extends AbstractDisplayer
{

    public function display(\Closure $callback = null, $apiUrl = '')
    {
        $callback = $callback->bindTo($this->row);

        list(
        $order_no,
        $subTotal,
        $receipt,
        $receiptTitle,
        $receiptTax,
        $requestPaperInvoice,
        ) = call_user_func($callback);

        $key = $this->getKey();

        $name = $this->column->getName();

        Admin::script($this->script($apiUrl));
        Admin::script("$('[data-key=\"{$key}\"]').popover()");

        return <<<EOT
<button type="button"
    class                    = "btn btn-secondary InvoiceState"
    title                    = ""
    data-key                 = "{$key}"
    data-orderno             = "{$order_no}"
    data-subtotal            = "{$subTotal}"
    data-receipt             = "{$receipt}"
    data-receipttitle        = "{$receiptTitle}"
    data-receipttax          = "{$receiptTax}"
    data-requestpaperinvoice = "{$requestPaperInvoice}"
    data-container           = "body"
    data-toggle              = "popover"
    data-placement           = "right"
    data-content             = "查詢中"
    >
  查詢
</button>

EOT;

    }


    protected function script($apiUrl)
    {
        return <<<EOT

        $('.InvoiceState').on('click', function() {

            var target              = $(this),
                key                 = target.data('key'),
                subTotal            = target.data('subtotal'),
                orderNo             = target.data('orderno'),
                invoicenumber       = target.data('orderno'),
                receipt             = target.data('receipt');
                receiptTitle        = target.data('receipttitle');
                receiptTax          = target.data('receipttax');
                requestPaperInvoice = target.data('requestpaperinvoice');

            $.ajax({
                url: '$apiUrl'+"/"+orderNo+"/"+subTotal+"/"+invoicenumber
            })
            .done(function(data) {
            var popoverTarget = target.context.attributes.getNamedItem("aria-describedby").value;

                popoverContent =  $('#'+popoverTarget+' .'+'popover-content');

                if(data.Status === "SUCCESS")
                {
                    var template = '狀態:已開立<br>發票資訊:'+receipt+'<br>發票抬頭:'+receiptTitle+'<br>統一編號:'+receiptTax+'<br>索取紙本發票:'+requestPaperInvoice+'<br>';

                    popoverContent.html(template);

                }
                else if(data.Status === "INV20006")
                {
                    popoverContent.html(data.Message);
                }
                else
                {
                    popoverContent.html('查詢失敗');
                }

                setTimeout(function(){ 
                    $('#'+popoverTarget).hide();
                }, 3000);
                
                console.log(data);
          });
  
});

EOT;
    }




}