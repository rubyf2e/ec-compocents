<?php

namespace App\Admin\Extensions;

use Encore\Admin\Form\Field;
use Encore\Admin\Form\Field\Text;

class SubTotal extends Text
{
    protected static $js = [
        '/vendor/laravel-admin/number-input/bootstrap-number-input.js',
    ];

    public function render()
    {
        $this->default((int) $this->default);

        $this->script = <<<EOT

$('#total:not(.initialized)')
    .addClass('initialized')
    .bootstrapNumber({
        upClass: 'success',
        downClass: 'primary',
        center: true
    });

    var totalParent     = $('#total').parent();
    var totalBtnSuccess = totalParent.find('.btn-success');
    var totalBtnPrimary = totalParent.find('.btn-primary');

    totalBtnSuccess.click(function(){
       value();
    })

    totalBtnPrimary.click(function(){
       value();
    })

    $('#total').keyup(function(){
        value();
    });

    var value = function(){
        var Total = $('#total').val();
        var Tax   = Total * 0.05;
            Tax   = Tax.toFixed(2);
        var Price = Total - Tax;
            Price = Price.toFixed(2);

        $('#tax').val(Tax);
        $('#price').val(Price);
    }

EOT;

        $this->prepend('')
        ->defaultAttribute('id', 'total')
        ->defaultAttribute('name', $this->elementName ?: $this->formatName($this->column))
        ->defaultAttribute('style', 'width: 100px');

        return parent::render();
    }
}
