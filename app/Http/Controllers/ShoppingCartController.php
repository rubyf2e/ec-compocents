<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\Model\Products as ProductsModel;
use App\Model\Orders as OrdersModel;

class ShoppingCartController extends Controller
{
    private $OrdersModel;
    private $ProductsModel;

    public function __construct()
    {
        $this->OrdersModel   = new OrdersModel();
        $this->ProductsModel = new ProductsModel();
    }


    public function shoppingInfo()
    {
      return view('shoppingcart/shoppingInfo');
    }

    public function shoppingOrder()
    {
      return view('shoppingcart/shoppingOrder');
    }

    /**
    * 更新購物車Session
    * 
    */
    public function updateSession(Request $request)
    {
      $data    = $request->input('results');
      $content = json_decode(Cart::content(),true);
          foreach ($data as $id => $value) {
            foreach ($content as $key => $item) {
                if($item['id'] === $id)
                {
                    Cart::update($item['rowId'], $data[$item['id']]['qty']);
                }
            }
        }

        return response()->json(Cart::content() , 200);
    }


    /**
    * 新增購物車Session
    * 
    */
    public function createSession(Request $request)
    {


        $data    = $request->input('results');
        $content = json_decode(Cart::content(), true);

        if(count($content) === 0)
        {
            //原本購物車是空的
            //就把session的加進去
            foreach ($data as $id => $value) {
                Cart::add([
                    [
                        'id'       => $value['id'],
                        'name'     => $value['name'],
                        'qty'      => $value['qty'],
                        'tax'      => $value['tax'],
                        'subTotal' => $value['subtotal'],
                        'price'    => $value['price']
                    ],
                ]);

            }

            return response()->json(Cart::content() , 200);
        }
        else
        {

            //如果購物車不是空的
            //就把新加的商品加進去，且傳進來的只會有一筆商品
            foreach ($content as $key => $item) {
                foreach ($data as $id => $value) {
                    if($item['id'] === $id)
                    {
                        Cart::update($item['rowId'], $item['qty'] + $data[$item['id']]['qty']);
                        return response()->json(Cart::content() , 200);
                    }
                    else
                    {
                        Cart::add([
                            [
                                'id'       => $value['id'],
                                'name'     => $value['name'],
                                'qty'      => $value['qty'],
                                'tax'      => $value['tax'],
                                'subTotal' => $value['subtotal'],
                                'price'    => $value['price']
                            ],
                        ]);
                        return response()->json(Cart::content() , 200);
                    }
                }
            }

        }


    }


    /**
    * 取得購物車Session
    * 
    */
    public function getSession()
    {
        $data  = Cart::content();

        return response()->json($data , 200);
    }
    

    /**
    * 產品列表
    * 
    */
    public function products($item_no = '')
    {
        $results = $this->ProductsModel->products($item_no);
        return response()->json($results , 200);
    }

    /**
    * 新增訂單
    * 
    */
    public function createOrders(Request $request)
    {
        $data            = $request->input('results');
        $MerchantOrderNo = $this->OrdersModel->createOrders($data);
        $results         = array(
            'Status'          => 'SUCCESS',
            'MerchantOrderNo' => $MerchantOrderNo
        );
        
        return response()->json($results , 200);
    }


    /**
    * 撈出對應訂單編號的資料
    * 
    */
    public function showOrders($MerchantOrderNo)
    {
        $results = $this->OrdersModel->showOrders($MerchantOrderNo);
        return response()->json($results , 200);
    }
}
