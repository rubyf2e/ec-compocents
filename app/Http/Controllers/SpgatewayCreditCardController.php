<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Database\QueryException;
use App\Services\Spgateway\SpgatewayCreditCardService;


/**
* spgateway
*智富通信用卡-請退款
* 
*/

class SpgatewayCreditCardController extends Controller
{
	protected $SpgatewayCreditCardService;
	private   $host;

	public function __construct()
	{
		$this->SpgatewayCreditCardService = new SpgatewayCreditCardService(config('custom.SpgatewayMPG_MerchantID'), config('custom.SpgatewayMPG_Hash_IV'), config('custom.SpgatewayMPG_Hash_Key'));
		$this->host                       = config('custom.SpgatewayCreditCardHost');
	}


	/**
	* 信用卡請退款
	* 
	*/
	public function close($CloseType, $MerchantOrderNo, $amount = '')
	{ 
		$method       = 'POST';
		$path         = 'API/CreditCard/Close';
		$url          = $this->host.$path;
		$results_data = $this->SpgatewayCreditCardService->close($url, $method, $CloseType, $MerchantOrderNo, $amount);
		return response()->json($results_data ,  200);

	}


}