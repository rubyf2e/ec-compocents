<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Database\QueryException;
use App\Services\Spgateway\SpgatewayMPGService;


/**
* spgateway
* 智富通金流
* 
*/

class SpgatewayMPGController extends Controller
{
	protected $SpgatewayMPGService;
	protected $SpgatewayService;
	private   $host;

	public function __construct()
	{
		$this->SpgatewayMPGService  = new SpgatewayMPGService(config('custom.SpgatewayMPG_MerchantID'), config('custom.SpgatewayMPG_Hash_IV'), config('custom.SpgatewayMPG_Hash_Key'));
		$this->host                 = config('custom.SpgatewayMPGHost');
	}


	/**
	* 查詢交易狀態
	* 
	*/
	public function checkData($MerchantOrderNo, $Amt)
	{ 
		$method           = 'POST';
		$path             = '/API/QueryTradeInfo';
		$url              = $this->host.$path;
		$results_data     = $this->SpgatewayMPGService->checkData($url, $method, $Amt, $MerchantOrderNo);
		return response()->json($results_data,  200);
	}


	/**
	* MPG串接
	* 
	*/
	public function mpg($MerchantOrderNo, $Amt, $ItemDesc = '', $Email)
	{ 
		$TradeInfoArray = $this->SpgatewayMPGService->mpg($MerchantOrderNo, $Amt, $ItemDesc, $Email);
		return response()->json($TradeInfoArray,  200);
	}


	/**
	* 
	* 交易後頁面
	* 
	*/
	public function mpgClientBack(Request $request)
	{ 
		echo var_dump($request->all());
	}


	/**
	*MPG串接支付完成返回商店網址 ReturnURL
	*
	*/
	public function mpgFeedback(Request $request)
	{ 
		$TradeInfoArray  = $this->SpgatewayMPGService->mpgFeedback($request);
		$TradeInfoResult = ($TradeInfoArray['Result']) ? ($TradeInfoArray['Result']) : array('Result' => 'default');

		return view('shoppingcart.mpgfeedback', [
			'Status'         => $TradeInfoArray['Status'],
			'Message'        => $TradeInfoArray['Message'],
			'TradeInfoArray' => json_encode($TradeInfoResult, true)
		]);

	}


	/**
	* MPG串接支付通知網址 NotifyURL
	* 
	*/
	public function mpgNotify(Request $request)
	{ 
		return $request;
	}


}