<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use App\Model\ChatBot;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Pusher\Pusher;
use App\Events\MessageSent;


class ChatBotController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
          $this->middleware('auth');  // 登录用户才能访问
        }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	return view('chatBot');
    }

    /**
     * Fetch all messages
     *
     * @return Message
     */
    public function fetchMessages()
    {
      return ChatBot::with('user')->get();
    }

    /**
     * Persist message to database
     *
     * @param  Request $request
     * @return Response
     */
    public function sendMessage(Request $request)
    {
      $user    = Auth::user();
      $message = $request->input('message');

      $data    =  array(
        'message'    => $message,
        'user_email' => $user->email,
        'user_name'  => $user->name
      );

      $user->ChatBotMessages()->create($data);

      broadcast(new MessageSent($user, $message))->toOthers();

      return response()->json($data,  200);
    }
  }
