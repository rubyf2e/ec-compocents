<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Tappay\TappayService;

class TapPayController extends Controller
{

    /*文件 https://docs.tappaysdk.com/tutorial/zh/web/front.html*/

    protected $TappayService;
    private $TappayPayByPrimeJson;


    public function __construct()
    {
        $this->TappayService = new TappayService();
        $this->data          = array(
            'card_key'     =>  '',
            'card_token'   =>  '',
            'order_number' =>  '',
            'details'      =>  '',
            'phone_number' =>  '',
            'amount'       =>  100,
            'name'         =>  '',
            'email'        =>  '',
            'zip_code'     =>  '',
            'address'      =>  '',
            'national_id'  =>  '',
        );
    }

    public function index()
    {
        return view('tappay');
    }


    /*前端所取得的 prime 字串進行交易,產生卡片識別字串及卡片金鑰*/
    public function TappayByPrime($prime = '', Request $request){
        $this->_TappayByPrime($prime, $request->input('data', $this->data)); 
        $results_data = $this->TappayService->api('tpc/payment/pay-by-prime', $this->TappayByPrimeData);
        return response()->json($results_data,  200);
    }

    /*查詢交易紀錄*/
    public function TappayRecord(Request $request){

        if($request->all())
        {
            $filters =
            [
                "filters" =>  
                [
                    "time"    =>  [
                        "start_time" =>  $request->input('start_time'),
                        "end_time"   =>  $request->input('end_time'),
                    ],
                    "amount" =>  [
                        "upper_limit" =>  $request->input('upper_limit'),
                        "lower_limit" =>  $request->input('lower_limit'),
                    ],
                    "cardholder" =>  [
                        "phone_number" =>  $request->input('phone_number'),
                        "name"         =>  $request->input('name'),
                        "email"        =>  $request->input('email'),
                    ],
                    'merchant_id'         => config('custom._TAPPAY_MERCHANT_ID'),
                    "record_status"       =>  $request->input('record_status'),
                    "rec_trade_id"        =>  $request->input('rec_trade_id'),
                    "order_number"        =>  $request->input('order_number'),
                    "bank_transaction_id" =>  $request->input('bank_transaction_id'),
                ]
            ];  
        }

        $TappayRecord = $this->TappayService->TappayRecord($filters);
        $results_data = $this->TappayService->api('tpc/transaction/query', $TappayRecord);
        return response()->json($results_data,  200);
    }

    /*綁定信用卡*/
    public function TappayBindCard(Request $request, $prime = ''){
        $cardholder = [
            "phone_number" =>  $request->input('phone_number'),
            "name"         =>  $request->input('name'),
            "email"        =>  $request->input('email'),
            "zip_code"     =>  $request->input('zip_code'),
            "address"      =>  $request->input('address'),
            "national_id"  =>  $request->input('national_id')
        ];

        $TappayBindCard = $this->TappayService->TappayBindCard($prime, $cardholder); 
        $results_data   = $this->TappayService->api('tpc/card/bind', $TappayBindCard);
        return response()->json($results_data,  200);
    }


    /*用TappayByPrime產生的卡片識別字串及卡片金鑰交易*/
    public function TappayByCardToken($prime = '', Request $request){
        $this->_TappayByPrime($prime, $request->input('data', $this->data));
        $results_data         = $this->TappayService->api('tpc/payment/pay-by-token', $this->TappayByPrimeData);
        return response()->json($results_data,  200);
    }

    /*移除綁定的 cardkey 及 cardtoken*/ 
    public function TappayRemoveCard($card_key, $card_token){
        $TappayRemoveCard = $this->TappayService->card_secret($card_key, $card_token);
        $results_data     = $this->TappayService->api('tpc/card/remove', $TappayRemoveCard);
        return response()->json($results_data,  200);
    }

    /*查詢該筆交易的詳細狀態*/ 
    public function TappayTradeHistory($rec_trade_id){
        $TappayTradeHistory = $this->TappayService->rec_trade_id($rec_trade_id);
        $results_data       = $this->TappayService->api('tpc/transaction/trade-history', $TappayTradeHistory);
        return response()->json($results_data,  200);
    }

    /*當天請款*/
    public function TappayCapToday($rec_trade_id){
        $TappayCapToday = $this->TappayService->rec_trade_id($rec_trade_id);  
        $results_data   = $this->TappayService->api('tpc/transaction/cap', $TappayCapToday);
        return response()->json($results_data,  200);
    }

    /*退款*/
    public function TappayRefund($rec_trade_id){
        $TappayRefund = $this->TappayService->rec_trade_id($rec_trade_id);
        $results_data = $this->TappayService->api('tpc/transaction/refund', $TappayRefund);
        return response()->json($results_data,  200);
    }

    public function _TappayByPrime($prime = '', $data = array())
    {
        $this->TappayByPrimeData    = $this->TappayService->TappayByPrime($prime, $data);
        $this->TappayPayByPrimeJson = json_encode($this->TappayByPrimeData);
    }

}

