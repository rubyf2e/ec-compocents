<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Database\QueryException;
use App\Services\Spgateway\SpgatewayInvoiceService;


/**
* spgateway
* 智富通電子發票
* 
*/

class SpgatewayInvoiceController extends Controller
{
	protected $SpgatewayInvoiceService;
	private   $host;

	public function __construct()
	{
		$this->SpgatewayInvoiceService = new SpgatewayInvoiceService(config('custom.SpgatewayInvoice_MerchantID_'), config('custom.SpgatewayInvoice_Hash_IV'), config('custom.SpgatewayInvoice_Hash_Key'));
		$this->host                    = config('custom.SpgatewayInvoiceHost');
	}


	/**
	* 查詢發票
	* 
	*/
	public function checkData($MerchantOrderNo, $TotalAmt = '', $InvoiceNumber = '', $RandomNum = '')
	{ 
		$method       = 'POST';
		$path         = '/API/invoice_search';
		$url          = $this->host.$path;
		$results_data =  $this->SpgatewayInvoiceService->checkData($url, $method, $MerchantOrderNo, $TotalAmt, $InvoiceNumber, $RandomNum);
		return response()->json($results_data ,  200);
	}


	/**
	* 開立發票參數設定(未觸發)
	* 
	*/
	public function invoice_issue($MerchantOrderNo, $type, Request $request)
	{ 
		$method       = 'POST';
		$path         = '/API/invoice_issue';
		$url          = $this->host.$path;
		$data         = $request->all();
		$results_data =  $this->SpgatewayInvoiceService->invoice_issue($url, $method, $MerchantOrderNo, $type, $data);
		return response()->json($results_data ,  200);

	}


	/**
	* 觸發已開立發票
	* 
	*/
	public function invoice_touch_issue($MerchantOrderNo, $InvoiceTransNo, $TotalAmt)
	{ 
		$method       = 'POST';
		$path         = '/API/invoice_touch_issue';
		$url          = $this->host.$path;
		$results_data =  $this->SpgatewayInvoiceService->invoice_touch_issue($url, $method, $MerchantOrderNo, $InvoiceTransNo, $TotalAmt);
		return response()->json($results_data ,  200);
	}

	/**
	* 開立折讓
	* 
	*/
	public function allowance_issue($MerchantOrderNo, $InvoiceNo, Request $request)
	{ 
		$method       = 'POST';
		$path         = '/API/allowance_issue';
		$url          = $this->host.$path;
		$data         = $request->all();
		$results_data =  $this->SpgatewayInvoiceService->allowance_issue($url, $method, $MerchantOrderNo, $InvoiceNo, $data);
		return response()->json($results_data,  200);
	}

	/**
	* 觸發確認折讓
	* 
	*/
	public function allowance_touch_issue($type, $MerchantOrderNo, $AllowanceNo, $AllowanceAmt)
	{ 
		$method       = 'POST';
		$path         = '/API/allowance_touch_issue';
		$url          = $this->host.$path;
		$results_data = $this->SpgatewayInvoiceService->allowance_touch_issue($url, $method, $type, $MerchantOrderNo, $AllowanceNo, $AllowanceAmt);
		return response()->json($results_data,  200);
	}

	/**
	* 作廢發票
	* 
	*/
	public function invoice_invalid($InvoiceNumber, $InvalidReason = '')
	{ 
		$method       = 'POST';
		$path         = '/API/invoice_invalid';
		$url          = $this->host.$path;
		$results_data =  $this->SpgatewayInvoiceService->invoice_invalid($url, $method, $InvoiceNumber, $InvalidReason);
		return response()->json($results_data,  200);
	}



}