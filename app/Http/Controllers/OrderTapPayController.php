<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Model\Tappay as TappayModel;
use App\Model\Member as MemberModel;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Services\Tappay\TappayService;

class OrderTapPayController extends TapPayController
{

    /*文件 https://docs.tappaysdk.com/tutorial/zh/web/front.html*/

    protected $TappayService;
    private $TappayModel;
    private $card_key   = '';
    private $card_token = '';
    private $TappayPayByPrimeJson;


    public function __construct()
    {
        $this->MemberModel   = new MemberModel();
        $this->TappayModel   = new TappayModel();
        $this->TappayService = new TappayService();
    }

    /*前端所取得的 prime 字串進行交易,產生卡片識別字串及卡片金鑰*/
    public function OrderTappayByPrime($id = '', $amount = '', $prime = '', $order_number = '', Request $request){
          $selectData = $this->MemberModel->where('id', $id)->first();
          $selectData = array(
            'card_key'     =>  $selectData->tappay_key,
            'card_token'   =>  $selectData->tappay_token,
            'order_number' =>  $order_number,
            'amount'       =>  $amount,
            'details'      =>  $request->input('details', '無'),
            'phone_number' =>  $selectData->celnum,
            'name'         =>  $selectData->fullname,
            'email'        =>  $selectData->email,
            'zip_code'     =>  $selectData->address_city_code,
            'address'      =>  $selectData->address_county_name.$selectData->address_city_name.$selectData->address_detail,
            'national_id'  =>  '',
        );

        $this->_TappayByPrime($prime, $selectData);
        $data      = array(
            'order_no'        => $request->input('order_number', ''),
            'auth_type'       => 'prime',
            'insert_datetime' => Carbon::now(),
            'status'          => -1,
            'msg'             => '',
            'rec_trade_id'    => '',
            'auth_code'       => '',
            'input'           => $this->TappayPayByPrimeJson,
            'output'          => '',
        );

        $client       = new Client();
        $results      = $client->request('POST', url('TapPay/TappayByPrime/' .$prime), [
            'form_params' => [
                'data' => $selectData,
            ]
        ]);

        $results_data = json_decode($results->getBody()->getContents(), true);

        $this->TappayModel->TappayByPrimeAddLog($data);

        $data         = array(
            'status'       => $results_data['status'],
            'msg'          => $results_data['msg'],
            'rec_trade_id' => $results_data['rec_trade_id'],
            'auth_code'    => $results_data['auth_code'],
            'auth_type'    => 'prime',
            'order_no'     => $this->TappayByPrime['order_number'],
            'input'        => $this->TappayPayByPrimeJson,
            'output'       => json_encode($results_data),
        );

        if(array_key_exists('card_secret', $results_data))
        {
            $card_secret['tappay_token'] = $results_data['card_secret']['card_token'];
            $card_secret['tappay_key']   = $results_data['card_secret']['card_key'];
            $this->MemberModel->where('id', $id)->update($card_secret);
        }

        $this->TappayModel->TappayByPrimeUpdateLog($data);
        return response()->json($results_data,  200);
    }

    public function _TappayByPrime($prime = '', $data = array())
    {
        $this->TappayByPrime        = $this->TappayService->TappayByPrime($prime, $data);
        $this->TappayPayByPrimeJson = json_encode($this->TappayByPrime);
    }
}

