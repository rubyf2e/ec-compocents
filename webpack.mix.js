
let mix = require('laravel-mix');
mix.browserSync({
	dir: ['../EC-components'],
	port:5007,
	proxy: 'ec-components',
	plugins: ["bs-html-injector"],
	online: true,
	injectChanges: true,
}).options({
 	extractVueStyles: true
 }).sourceMaps()
 .copyDirectory('resources/images', 'public/images')
 .js('resources/assets/js/TapPay.js', 'public/js')
 .js('resources/assets/js/app.js', 'public/js').version().extract(['vue'])
 .sass('resources/assets/sass/app.scss', 'public/css', {
    strictMath: true
});