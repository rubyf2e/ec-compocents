
# 電商基礎元組件


##  前端 vue元組件
元組件資料夾

路徑 `resources/assets/js/components`


### 首頁 / 幻燈片

```
Vue.component('SlickSlider', require('./components/Sliders/SlickSlider.vue'));
Vue.component('VueCarouselSlider', require('./components/Sliders/VueCarouselSlider.vue'));
```

### 商品頁 /Products 
```
Vue.component('products', require('./components/Products/ProductsPage.vue'));
```

### 產品全部列表
```
Vue.component('shopping-order', require('./components/ShoppingOrder/ShoppingOrder.vue'));
```

### 單一產品元件
```
Vue.component('order-item', require('./components/ShoppingOrder/ShoppingOrderItem.vue'));
```

### TapPay 信用卡
```
Vue.component('TapPayForm', require('./components/TapPay/form.vue'));
```

### 智富通交易完成回呼組件
```
Vue.component('mpgfeedback', require('./components/MpgFeedBack/MpgFeedBack.vue'));
```

### 訂購人資料頁面 /ShoppingCart/shoppingInfo
父組件
```
Vue.component('shopping-info', require('./components/ShoppingInfo/ShoppingInfo.vue'));
```

子組件

姓名
```
Vue.component('shopping-name', require('./components/ShoppingInfo/ShoppingName.vue'));
```

收件人
```
Vue.component('shopping-recipient', require('./components/ShoppingInfo/ShoppingRecipient.vue'));
```

Email
```
Vue.component('shopping-email', require('./components/ShoppingInfo/ShoppingEmail.vue'));
```

聯絡電話
```
Vue.component('shopping-tel', require('./components/ShoppingInfo/ShoppingTel.vue'));
```

訂單備註 
```
Vue.component('shopping-comment', require('./components/ShoppingInfo/ShoppingComment.vue'));
```

配送方式
```
Vue.component('shopping-shipping', require('./components/ShoppingInfo/ShoppingShipping.vue'));
```

發票資訊
```
Vue.component('shopping-receipt', require('./components/ShoppingInfo/ShoppingReceipt.vue'));
```

### 智富通金流 mpgApi 傳遞值
```
Vue.component('shopping-mpgApi', require('./components/ShoppingInfo/ShoppingMpgApi.vue'));
```

### 結帳購物車列表
父組件
```
Vue.component('shopping-list', require('./components/ShoppingInfo/ShoppingList.vue'));
```
子組件
```
Vue.component('shopping-listItem', require('./components/ShoppingInfo/ShoppingListItem.vue'));
```

### 購物車
父組件
```
Vue.component('shopping-cart', require('./components/ShoppingCart/ShoppingCart.vue'));
```
子組件
```
Vue.component('cart-item', require('./components/ShoppingCart/ShoppingCartItem.vue'));
```


##  後台

+ 訂單列表-提供訂單及發票狀態查詢 `/admin/orders`


+ 產品列表-提供產品查詢及設定 `/admin/products`


+ 匯出訂單-匯出訂單狀態為已成立(已刷卡)的訂單 `/admin/order`


+ 開立發票 `/admin/Invoice`



##  schedule 排程

七天內有效訂單請款 
```
$schedule->command('route:call /api/Order/requestPayment/1')->dailyAt('09:00')->appendOutputTo(storage_path('daily-settle.log'));
```

撈取7-11 FTP資料
```
$schedule->command('route:call /api/Order/FTP711')->dailyAt('10:00')->appendOutputTo(storage_path('FTP711.log'));
```

每日寄出有效訂單
```
$schedule->command('route:call /api/Order/daily')->dailyAt('15:10');
```

##  API
+ 驗證 X-Seed X-User X-Sign 相關api接口程式 `appfeedback` 
  + 驗證X-seed後接收app客戶回饋信件的資料並回覆status給app端，同時寫入資料庫，寄出信件 `appfeedback/create`

+ 智富通金流官方連結
	+ 正式站 https://www.spgateway.com/
	+ 測試站 https://ccore.spgateway.com/

+ 智付寶電子發票官方連結
	+ 正式站 https://inv.pay2go.com/
	+ 測試站 https://cinv.pay2go.com/

+ 智富通 金流API `SpgatewayMPG`
  + 查詢交易狀態 `SpgatewayMPG/checkData/{MerchantOrderNo}/{Amt}`
  + MPG串接     `SpgatewayMPG/mpg/{MerchantOrderNo}/{Amt}/{ItemDesc}/{Email}`
  + 交易後頁面   `SpgatewayMPG/mpgClientBack`
  + MPG串接支付完成返回商店網址 ReturnURL  `SpgatewayMPG/mpgFeedback`
  + MPG串接支付通知網址 NotifyURL   `SpgatewayMPG/mpgNotify`

+ 智富通 發票API `SpgatewayInvoice`
  + 查詢發票 `SpgatewayInvoice/checkData/{MerchantOrderNo}/{TotalAmt}/{InvoiceNumber}`
  + 開立發票參數設定(未觸發) `SpgatewayInvoice/invoice_issue/{MerchantOrderNo}`
  + 觸發已開立發票 `SpgatewayInvoice/invoice_touch_issue/{MerchantOrderNo}/{InvoiceTransNo}/{TotalAmt}`
  + 開立折讓 `SpgatewayInvoice/allowance_issue/{MerchantOrderNo}/{InvoiceNo}`
  + 觸發確認折讓 `SpgatewayInvoice/allowance_touch_issue/{type}/{MerchantOrderNo}/{AllowanceNo}/{AllowanceAmt}`

+ 智富通信用卡 請退款API `SpgatewayCreditCard`
	+ 信用卡請退款 `SpgatewayCreditCard/close/{CloseType}/{MerchantOrderNo}/{amount}`

+ TapPay 信用卡API官方文件
	+ 前端 https://docs.tappaysdk.com/tutorial/zh/web/front.html
	+ 後端 https://docs.tappaysdk.com/tutorial/zh/back.html
	+ 管理後台 https://portal.tappaysdk.com/

+ TapPay 信用卡API `TapPay`
	+ 測試頁面 `TapPay`
	+ 前端所取得的 prime 字串進行交易,產生卡片識別字串及卡片金鑰 `TapPay/TappayByPrime/{prime}`
	+ 用TappayByPrime產生的卡片識別字串及卡片金鑰交易 `TapPay/TappayByCardToken`
	+ 退款 `TapPay/TappayRefund/{rec_trade_id}`
	+ 查詢交易紀錄 `TapPay/TappayRecord`
	+ 當天請款 `TapPay/TappayCapToday/{rec_trade_id}`
	+ 綁定信用卡 `TapPay/TappayBindCard`
	+ 移除綁定的 cardkey 及 cardtoken `TapPay/TappayRemoveCard/{card_key}/{card_token}`
	+ 查詢該筆交易的詳細狀態 `TapPay/TappayTradeHistory/{rec_trade_id}`

+ 購物車 `ShoppingCart`
	+ 產品列表 `ShoppingCart/shoppingOrder` 
	+ 訂購人資料 `ShoppingCart/shoppingInfo` 
	+ 取得購物車Session `ShoppingCart/getSession` 
	+ 更新購物車Session `ShoppingCart/updateSession`
	+ 新增購物車Session `ShoppingCart/createSession` 
	+ 新增訂單 `ShoppingCart/createOrders` 
	+ 產品列表 `ShoppingCart/products` 
	+ 單一產品 `ShoppingCart/products/{item_no}` 
	+ 撈出對應訂單編號的資料 `ShoppingCart/showOrders/{MerchantOrderNo}`


