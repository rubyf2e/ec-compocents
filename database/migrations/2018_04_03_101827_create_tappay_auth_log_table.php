<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTappayAuthLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tappay_auth_log', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_no')->length(16);
            $table->string('auth_type')->length(5);
            $table->integer('status')->length(11);
            $table->string('msg')->length(200);
            $table->string('rec_trade_id')->length(20);
            $table->string('auth_code')->length(6);
            $table->text('input');
            $table->text('output');
            $table->timestamp('insert_datetime');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tappay_auth_log');
    }
}
