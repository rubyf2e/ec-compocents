<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpgatewayOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('spgateway_orders');
        Schema::create('spgateway_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('orders_id')->length(200);
            $table->string('firstName')->length(200);
            $table->string('lastName')->length(200);
            $table->string('name')->length(200);
            $table->string('recipient')->length(200);
            $table->string('email')->length(200);
            $table->string('tel')->length(200);
            $table->integer('shipping')->length(1);
            $table->string('address')->length(200);
            $table->integer('storeSelect')->length(1);
            $table->integer('receipt')->length(1);
            $table->string('receiptTitle')->length(200);
            $table->string('receiptTax')->length(200);
            $table->boolean('requestPaperInvoice');
            $table->text('comment')->length(200);
            $table->json('cartContent');
            $table->integer('subTotal')->length(10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spgateway_orders');
    }
}
