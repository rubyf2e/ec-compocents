<?php
Use App\Model\Orders;
use Illuminate\Database\Seeder;

class OrdersSeeder extends Seeder
{
     /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
     	$target = array(
     		array(
     			"orders_id" => 'NduPQ',
     			"amount"    => '52471',
     		),
                    array(
                    "orders_id" => '5qr21',
                    "amount"    => '101850',
               ),
                    array(
                    "orders_id" => 'kpJQX',
                    "amount"    => '38033',
               ),
                    array(
                    "orders_id" => 'aWKau',
                    "amount"    => '41934',
               ),
                    array(
                    "orders_id" => 'IlhRT',
                    "amount"    => '20867',
               ),

                    array(
                    "orders_id" => 'NduPQ',
                    "amount"    => '52471',
               ),
                    array(
                    "orders_id" => 'Nj01s',
                    "amount"    => '63761',
               ),
                    array(
                    "orders_id" => '7Qble',
                    "amount"    => '25894',
               ),
                    array(
                    "orders_id" => 'qGxAJ',
                    "amount"    => '40973',
               ),
                    array(
                    "orders_id" => 'IY708',
                    "amount"    => '45490',
               )
     	);
     	for ($i=0; $i < count($target) ; $i++) { 
     		Orders::create([
     			'orders_id' => $target[$i]['orders_id'],
     			'amount'    => $target[$i]['amount']
     		]);
     	}
     }
   }