<?php
Use App\Model\Admin_menu;
use Illuminate\Database\Seeder;
class Admin_menuTableSeeder extends Seeder
{
     /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
     	$target = array(
     		array(
     			"parent_id" => 0,
     			"order"     => 1,
     			"title"     => "訂單列表",
     			"icon"      => "fa-book",
     			"uri"       => "orders",
     		),
               array(
                    "parent_id" => 0,
                    "order"     => 2,
                    "title"     => "產品列表",
                    "icon"      => "fa-archive",
                    "uri"       => "products",
               ),
               array(
                    "parent_id" => 0,
                    "order"     => 3,
                    "title"     => "匯出訂單",
                    "icon"      => "fa-book",
                    "uri"       => "order",
               ),
               array(
                    "parent_id" => 0,
                    "order"     => 4,
                    "title"     => "開立發票",
                    "icon"      => "fa-usd",
                    "uri"       => "Invoice",
               ),
               array(
                    "parent_id" => 0,
                    "order"     => 5,
                    "title"     => "腳踏車開通密碼重置",
                    "icon"      => "fa-motorcycle",
                    "uri"       => "Bike",
               ),
               array(
                    "parent_id" => 0,
                    "order"     => 6,
                    "title"     => "後台設定",
                    "icon"      => "fa-tasks",
                    "uri"       => "",
               ),
               array(
                    "parent_id" => 2,
                    "order"     => 7,
                    "title"     => "管理員",
                    "icon"      => "fa-users",
                    "uri"       => "auth/users",
               ),
               array(
                    "parent_id" => 2,
                    "order"     => 8,
                    "title"     => "角色",
                    "icon"      => "fa-user",
                    "uri"       => "auth/roles",
               ),
               array(
                    "parent_id" => 2,
                    "order"     => 9,
                    "title"     => "權限",
                    "icon"      => "fa-ban",
                    "uri"       => "auth/permissions",
               ),
               array(
                    "parent_id" => 2,
                    "order"     => 10,
                    "title"     => "目錄",
                    "icon"      => "fa-bars",
                    "uri"       => "auth/menu",
               ),
               array(
                    "parent_id" => 2,
                    "order"     => 11,
                    "title"     => "登入資料",
                    "icon"      => "fa-history",
                    "uri"       => "auth/logs",
               ),

          );
     	for ($i=0; $i < count($target) ; $i++) { 
     		Admin_menu::create([
     			'parent_id' => $target[$i]['parent_id'],
     			'order'     => $target[$i]['order'],
     			'title'     => $target[$i]['title'],
     			'icon'      => $target[$i]['icon'],
     			'uri'       => $target[$i]['uri'],
     		]);
     	}
     }
}