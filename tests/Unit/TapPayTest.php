<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class TapPayTest extends TestCase
{
  use WithFaker;
  private $rec_trade_id;
  private $card_key;
  private $card_token;
  private $filters =
  [
    "filters" =>  
    [
      "time"    =>  [
        "start_time" =>  '',
        "end_time"   =>  '',
      ],
      "amount" =>  [
        "upper_limit" =>  '',
        "lower_limit" =>  '',
      ],
      "cardholder" =>  [
        "phone_number" =>  '',
        "name"         =>  '',
        "email"        =>  '',
      ],
      'merchant_id'         => 'yonglinfarm_TAISHIN',
      "record_status"       =>  '',
      "rec_trade_id"        =>  '',
      "order_number"        =>  '',
      "bank_transaction_id" =>  ''
    ]
  ];

  private $cardholder = [
    "phone_number" =>  "+886923456789",
    "name"         =>  "王小明",
    "email"        =>  "LittleMing@Wang.com",
    "zip_code"     =>  "100",
    "address"      =>  "台北市天龍區芝麻街1號1樓",
    "national_id"  =>  "A123456789"
  ];

  private $data = [
      'card_key'     =>  '',
      'card_token'   =>  '',
      'order_number' =>  'test',
      'details'      =>  '無',
      'amount'       =>  100,
      'phone_number' =>  'test',
      'name'         =>  'test',
      'email'        =>  'test',
      'zip_code'     =>  'test',
      'address'      =>  'test',
      'national_id'  =>  'test',
  ];

  /*前端所取得的 prime 字串進行交易,產生卡片識別字串及卡片金鑰*/
  public function testTappayByPrime(){
    $this->TappayByPrime();
  }

  /*用TappayByPrime產生的卡片識別字串及卡片金鑰交易*/
  public function testTappayByCardToken(){
    $this->TappayByPrime();
    $this->TappayByCardToken();
  }

  /*退款*/
  public function testTappayRefund(){
    $this->TappayByPrime();
    $this->TappayRefund();
  }

  /*查詢交易紀錄*/
  public function testTappayRecord(){
    $this->TappayRecord();
  }

  /*當天請款*/
  public function testTappayCapToday(){
    $this->TappayByPrime();
    $this->TappayCapToday();
  }


  /*綁定信用卡*/
  public function testTappayBindCard(){
    $this->TappayBindCard();
  }

  /*移除綁定的 cardkey 及 TappayRemoveCard*/ 
  public function testTappayRemoveCard(){
    $this->TappayByPrime();
    $this->TappayRemoveCard();
  }

  /*能讓您查詢該筆交易的詳細狀態*/ 
  public function testTappayTradeHistory(){
    $this->TappayByPrime();
    $this->TappayTradeHistory();
  }

  /*前端所取得的 prime 字串進行交易,產生卡片識別字串及卡片金鑰*/
  private function TappayByPrime(){
    $response = $this->get('/TapPay/TappayByPrime/');
    $result   = $response->assertStatus(200)->assertJson([
      'msg'     => 'Success'
    ]);

    $this->rec_trade_id = $response->original['rec_trade_id'];
    $this->card_key     = $response->original['card_secret']['card_key'];
    $this->card_token   = $response->original['card_secret']['card_token'];
  }

  /*用TappayByPrime產生的卡片識別字串及卡片金鑰交易*/
  private function TappayByCardToken(){
    $data               = $this->data;
    $data['card_key']   = $this->card_key;
    $data['card_token'] = $this->card_token;
    $response           = $this->json('POST', '/TapPay/TappayByCardToken/', array('data' => $data));
    $result             = $response->assertStatus(200)->assertJson([
    'msg'     => 'Success'
    ]);
  }

  /*退款*/
  private function TappayRefund(){
    $response = $this->get('/TapPay/TappayRefund/'. $this->rec_trade_id);
    $result   = $response->assertStatus(200)->assertJson([
      'msg'     => 'Success'
    ]);
  }

  /*查詢交易紀錄*/
  private function TappayRecord(){
    $response = $this->get('/TapPay/TappayRecord/');
    $response = $this->json('POST', '/TapPay/TappayRecord/', $this->filters);
    $result   = $response->assertStatus(200)->assertJson([]);
  }

  /*當天請款*/
  private function TappayCapToday(){
    $response = $this->get('/TapPay/TappayCapToday/'. $this->rec_trade_id);
    $result   = $response->assertStatus(200)->assertJson([
      'msg'     => 'Success'
    ]);
  }

  /*綁定信用卡*/
  private function TappayBindCard(){
    $response = $this->json('POST', '/TapPay/TappayBindCard/', $this->cardholder);
    $result   = $response->assertStatus(200)->assertJson([
      'msg'     => 'Success'
    ]);
  }

  /*移除綁定的 cardkey 及 TappayRemoveCard*/ 
  private function TappayRemoveCard(){
    $response = $this->get('/TapPay/TappayRemoveCard/' .$this->card_key. '/' .$this->card_token);
    $result   = $response->assertStatus(200)->assertJson([
      'msg'     => 'Success'
    ]);
  }

  /*能讓您查詢該筆交易的詳細狀態*/ 
  private function TappayTradeHistory(){
    $response = $this->get('/TapPay/TappayTradeHistory/'. $this->rec_trade_id);
    $result   = $response->assertStatus(200)->assertJson([
      'msg'     => 'Success'
    ]);
  }


}
