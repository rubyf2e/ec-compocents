<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class AppfeedbackTest extends TestCase
{

			use WithFaker;

			/**
			* 測試未帶驗證
			*/
			public function testNoHeader()
			{
				$response = $this->json('POST', '/api/appfeedback/create');
				$response->assertStatus(200)->assertExactJson([
					'status' => false,
				]);
			}


			/**
			* 測試不輸入值
			*/
			public function testNoValue()
			{
				$response = $this
				->withHeaders(
					[
						'X-Seed' => '1514959701.54',
						'X-Sign' => 'cvHRmiiW/Bi0W0spcxHUc+jCS58=',
					]
				)
				->json('POST', '/api/appfeedback/create');
				$response->assertStatus(200)->assertExactJson([
					'status'       => false,
					'errorCode'    => '000001',
					'errorMessage' => '資料未填寫完整'
				]);
			}

			/**
			* 測試必填參數
			*/
			public function testValueInsert()
			{

				$response = $this
				->withHeaders(
					[
						'X-Seed' => '1514959701.54',
						'X-Sign' => 'cvHRmiiW/Bi0W0spcxHUc+jCS58=',
					]
				)
				->json('POST', '/api/appfeedback/create', 
					[
						'description' =>  $this->faker()->name,
						'title'       =>  $this->faker()->name,
						'user_id'     => '1111',
						'id'          => str_random(3)
					]);

				$response->assertStatus(200)->assertJson([
					'status' => true,
				]);
			}

		}
