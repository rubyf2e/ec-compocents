<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class SpgatewayMPGTest extends TestCase
{
	use WithFaker;
	private $MerchantOrderNo = '100013ddd333';
	private $Amt             = '10000';

    /**
     * 檢查checkData api回覆
     *
     * @return void
     */
    public function testCheckDataApiget()
    {
    	$response = $this->get('/api/SpgatewayMPG/checkData/' .$this->MerchantOrderNo. '/' .$this->Amt. '/');

    	$response->assertStatus(200)->assertJson([
    		'Result' => [],
    	]);
    }

    /**
     * 檢查mpg Web頁面是否正常
     * 
     * @return void
     */
    public function testMpgWebget()
    {
    	$response = $this->get('/api/SpgatewayMPG/mpg/' .str_random(3). '/' .$this->faker()->randomDigitNotNull. '/' .$this->faker()->name. '/' .$this->faker()->email. '/');

    	/*Version= 1.4*/
    	$response->assertStatus(200)->assertSee('1.4');
    }


    /**
     * 檢查mpgFeedback 回覆
     *
     * @return void
     */
    public function testMpgFeedbackWebPost()
    {

    	// $response = $this
    	// ->json('POST', '/SpgatewayMPG/mpgFeedback/', 
    	// 	[
    	// 		'TradeInfo' =>  '24a484e0f73bd68f5c2185c38f44498a99d7e06057fa5090a1b34b2125dc954e5f6daa4ae87c712eb3fb30ccc470a694011d95df2ff0c9e5d5a7a880d42dd436e8b3aa96e5df5610f263ba5e35a2821620441a12ccb72a863034b6b78c128f8d365a6bf946091a58e509e1e2f788a964351c000e7e6a07f0601d3fff448abd758a52b43eda92b34b93638bc8b69f3dff4fe744d539f3cacf30c69bfcf2948e70892b8484274507198bdbae408389443aa3d8b08314c302d299dde4422d3fcbb391ea38b37d61a549ef83e135045969f5cc15476dc8c66440a0de25d6ca3ae6315adb271de61a85a3443d118cdfd867492c5df7e1dfcaba63a6a591a38fbf9b79b9475d8bb9e9f432b079318d96401a7ced30bc8f6988bf05fbec47ee034e9707e081ece115262277eb2decf5430ef78eca67e3fbe21b88bb94732aa89a5e99de8c33fcdac7f121aa330fc35e88367024274e6948101bec8efb08d5331ad2e6628f954484758653a5f9862b7d3f2a125f1901db88918dedd6df9ec5dcf6985f006c9ec9cfcfe21f19a04388191cee7ec120e9ac755ac6351c3aed9a3042b0833c9bf39841db652109b0befd3b1a04188240f1abb74bb2d85a32f5a7329c4af4b9'
    	// 	]);


    	// $response->assertStatus(200)->assertSee('Status:SUCCESS');
    }


  }
