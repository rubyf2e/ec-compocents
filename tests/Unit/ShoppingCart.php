<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class ShoppingCart extends TestCase
{

	use WithFaker;
	private $ordersArray = array('results' => array(
		'firstName'           => 'firstName',
		'lastName'            => 'lastName',
		'name'                => 'name',
		'recipient'           => 'recipient',
		'email'               => 'email',
		'tel'                 => 'tel',
		'shipping'            => 1,
		'address'             => 'address',
		'storeSelect'         => 0,
		'receipt'             => 1,
		'receiptTitle'        => 'receiptTitle',
		'receiptTax'          => 'receiptTax',
		'requestPaperInvoice' => false,
		'comment'             => 'comment',
		'cartContent'         => '{}',
		'subTotal'            => 0,
	));


		/**
	   * 測試 createOrders api 新增訂單 1筆
	   * 
	   *
	   * @return void
	   */
		public function testcreateOrdersApi()
		{
			$this->createOrdersApi();
		}

		/**
	   * 測試 createOrders api 新增訂單 20筆
	   * 
	   *
	   * @return void
	   */
		public function testcreateOrders20Api()
		{
			for ($i=0; $i < 20; $i++) { 
				$this->createOrdersApi();
			}
		}


		/**
     * 測試 createOrders api 新增訂單
     *
     * @return void
     */
		public function createOrdersApi()
		{
			$response = $this->json('POST', '/api/shoppingCart/createOrders/', $this->ordersArray);
			$response->assertStatus(200)->assertJson([
					'Status'          => 'SUCCESS'
				]);

		}


	}
