<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class SpgatewayInvoiceIssueTest extends TestCase
{
	use WithFaker;
  private $MerchantOrderNo = 'test12';
  private $Amt             = '100';
  private $InvoiceNumber   = 'AA00000006';
  private $InvoiceTransNo  = '18012316094394499';
  private $AllowanceNo     = 'A180123161530805';
  private $TotalAmt        = '100';
  private $AllowanceAmt    = '100';
  private $SalesTax        = 0.05;
  private $PostData_array  = [];
  private $ItemCount0      = 0;
  private $ItemPrice0      = 0;
  private $ItemAmt0        = 0;
  private $ItemCount1      = 0;
  private $ItemPrice1      = 0;
  private $ItemAmt1        = 0;


    /**
     * 測試二聯 開立發票->觸發發票->作廢發票
     *
     * @return void
     */
    public function test2Invoice_issue_to_invoice_invalid()
    {
     $this->Invoice_issue_to_invoice_invalid(2);
   }


     /**
     * 測試三聯 開立發票->觸發發票->作廢發票
     *
     * @return void
     */
     public function test3Invoice_issue_to_invoice_invalid()
     {
      $this->Invoice_issue_to_invoice_invalid(3);

    }


    /**
     * 開立二聯發票->開立折讓->取消折讓->開立折讓->觸發確認折讓
     *
     * @return void
     */
    public function testInvoice_issue_to_allowance_touch_issue(){
      $this->MerchantOrderNo = str_random(5);
      $this->invoice_issue(2);
      $this->invoice_touch_issue();
      $this->allowance_issue();
      $this->allowance_touch_issue('D');
      $this->allowance_issue();
      $this->allowance_touch_issue('C');
    }

    /**
     * 測試二聯 開立發票->觸發發票->作廢發票 轉 三聯 開立發票->觸發發票->作廢發票
     *
     * @return void
     */
    public function testInvoice_issue_2_to_3(){
      $this->Invoice_issue_to_invoice_invalid(2);
      $this->Invoice_issue_to_invoice_invalid(3);
    }

    /**
     * 開立二聯發票->開立折讓->觸發確認折讓->開立三聯發票->觸發發票->作廢發票
     *
     * @return void
     */
    public function testInvoice_issue2_to_allowance_touch_issue_to_Invoice_issue3(){
      $this->MerchantOrderNo = str_random(5);
      $this->invoice_issue(2);
      $this->invoice_touch_issue();
      $this->allowance_issue();
      $this->allowance_touch_issue('C');
      $this->Invoice_issue_to_invoice_invalid(3);
    }


    /**
     * 發票資料
     *
     * @return void
     */
    private function returnInvoiceIssueArray($type){
      $this->PostData_array = array(
        'type'         => $type,
        'BuyerUBN'     => $this->faker()->numberBetween(54111111, 54999999),
        'BuyerName'    => $this->faker()->name,
        'BuyerAddress' => $this->faker()->name,
        'BuyerEmail'   => $this->faker()->email,
        'BuyerPhone'   => $this->faker()->numberBetween(100000000, 900000000),
        'Comment'      => '信用卡末四碼:' .$this->MerchantOrderNo, 
        'ItemName'     => '',
        'ItemCount'    => '',
        'ItemUnit'     => '',
        'ItemPrice'    => '',
        'ItemAmt'      => ''
      );

      $this->returnOrder_items();
      return $this->PostData_array;
    }


    /**
     * 折讓資料
     *
     * @return void
     */
    private function returnAllowanceIssueArray(){
      return array(
        'ItemName'     => $this->PostData_array['ItemName'],
        'ItemCount'    => $this->PostData_array['ItemCount'],
        'ItemUnit'     => $this->PostData_array['ItemUnit'],
        'ItemPrice'    => $this->PostData_array['ItemPrice'],
        'ItemAmt'      => $this->PostData_array['ItemAmt'],
        'ItemTaxAmt'   => "0|0",
        'TotalAmt'     => $this->PostData_array['TotalAmt'],
      );
    }
    


    /**
     * return 商品陣列
     *
     * @return void
     */
    private function returnOrder_items(){
      $this->ItemCount0 = $this->faker()->randomDigitNotNull;
      $this->ItemPrice0 = $this->faker()->numberBetween(1000, 10000);
      $this->ItemAmt0   = $this->ItemCount0 * $this->ItemPrice0;


      $this->ItemCount1 = $this->faker()->randomDigitNotNull;
      $this->ItemPrice1 = $this->faker()->numberBetween(1000, 10000);
      $this->ItemAmt1   = $this->ItemCount1 * $this->ItemPrice1;


      $order_items = array(
        0 => array(       
          'ItemName'  => $this->faker()->name,
          'ItemCount' => $this->ItemCount0,
          'ItemUnit'  => '',
          'ItemPrice' => $this->ItemPrice0,
          'ItemAmt'   => $this->ItemAmt0
        ),
        1 => array(       
          'ItemName'  => $this->faker()->name,
          'ItemCount' => $this->ItemCount1,
          'ItemUnit'  => '',
          'ItemPrice' => $this->ItemPrice1,
          'ItemAmt'   => $this->ItemAmt1
        ),
      );

      foreach ($order_items as $key => $order_item) {
        if (0 != $key) {
          $this->PostData_array['ItemName']  .= '|';
          $this->PostData_array['ItemCount'] .= '|';
          $this->PostData_array['ItemUnit']  .= '|';
          $this->PostData_array['ItemPrice'] .= '|';
          $this->PostData_array['ItemAmt']   .= '|';
        }
        $this->PostData_array['ItemName']  .= $order_item['ItemName'];
        $this->PostData_array['ItemCount'] .= $order_item['ItemCount'];
        $this->PostData_array['ItemUnit']  .= '件';
        $this->PostData_array['ItemPrice'] .= $order_item['ItemPrice'];
        $this->PostData_array['ItemAmt']   .= $order_item['ItemAmt'];
      }

      $this->TotalAmt = $this->ItemAmt0 + $this->ItemAmt1;
      $TaxAmt         = round($this->TotalAmt * $this->SalesTax);
      $Amt            = $this->TotalAmt - $TaxAmt;

      
      $this->PostData_array['Amt']      = strval($Amt);
      $this->PostData_array['TaxAmt']   = strval($TaxAmt);
      $this->PostData_array['TotalAmt'] = strval($this->TotalAmt);
    } 


    /**
     * 開立發票->觸發發票->作廢發票
     *
     * @return void
     */
    private function Invoice_issue_to_invoice_invalid($type){
      $this->MerchantOrderNo = str_random(5);
      $this->invoice_issue($type);
      $this->invoice_touch_issue();
      $this->invoice_invalid();
    }


    /**
     * 開立折讓
     *
     * @return void
     */
    private function allowance_issue()
    {
      $response = $this->json('POST', '/api/SpgatewayInvoice/allowance_issue/' .$this->MerchantOrderNo. '/' .$this->InvoiceNumber, $this->returnAllowanceIssueArray());
      $response->assertStatus(200)->assertJson([
        'Status' => 'SUCCESS',
        'Result' => [],
      ]);

      $this->AllowanceNo  = $response->original['Result']['AllowanceNo'];
      $this->AllowanceAmt = $response->original['Result']['AllowanceAmt'];
    }


    /**
    * 觸發確認折讓
    * allowance_touch_issue
    * C = 確認折讓。
    * D = 取消折讓。
    * 
    */
    private function allowance_touch_issue($type)
    {
      $response = $this->post('/api/SpgatewayInvoice/allowance_touch_issue/' .$type. '/' .$this->MerchantOrderNo. '/' .$this->AllowanceNo. '/' .$this->AllowanceAmt);
      $response->assertStatus(200)->assertJson([
        'Status' => 'SUCCESS',
        'Result' => [],
      ]);
    }

    /**
     * 開立發票
     *
     * @return void
     */
    private function invoice_issue($type){
     $response = $this->json('POST', '/api/SpgatewayInvoice/invoice_issue/' .$this->MerchantOrderNo, 
      $this->returnInvoiceIssueArray($type));

     $result = $response->assertStatus(200)->assertJson([
      'Status' => 'SUCCESS',
      'Result' => [],
    ]);

     $this->InvoiceTransNo = $response->original['Result']['InvoiceTransNo'];

   }

    /**
     * 觸發發票
     *
     * @return void
     */
    private function invoice_touch_issue(){
      $response = $this->get('/api/SpgatewayInvoice/invoice_touch_issue/' .$this->MerchantOrderNo. '/' .$this->InvoiceTransNo. '/' .$this->TotalAmt. '/');
      $response->assertStatus(200)->assertJson([
        'Status' => 'SUCCESS',
        'Result' => [],
      ]);

      $this->InvoiceNumber = $response->original['Result']['InvoiceNumber'];
    }


    /**
     * 作廢發票
     *
     * @return void
     */
    private function invoice_invalid(){
     $response = $this->get('/api/SpgatewayInvoice/invoice_invalid/' .$this->InvoiceNumber. '/test/');
     $response->assertStatus(200)->assertJson([
      'Status' => 'SUCCESS',
      'Result' => [],
    ]);
   }

 }
