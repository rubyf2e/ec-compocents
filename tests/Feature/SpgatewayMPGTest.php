<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SpgatewayMPGTest extends TestCase
{

	use WithFaker;
	private $MerchantOrderNo = '100013ddd333';
	private $Amt             = '10000';

  	/**
     * 檢查checkData api格式
     *
     * @return void
     */
  	public function testCheckDataApiPost()
  	{
  		$response = $this->post('/api/SpgatewayMPG/checkData/' .$this->MerchantOrderNo. '/' .$this->Amt. '/');

  		$response->assertStatus(405);
  	}

    /**
     * 檢查checkData api格式
     *
     * @return void
     */
    public function testCheckDataApiget()
    {
    	$response = $this->get('/api/SpgatewayMPG/checkData/' .$this->MerchantOrderNo. '/' .$this->Amt. '/');

    	$response->assertStatus(200);
    }

    /**
     * 檢查mpg api格式
     *
     * @return void
     */
    	public function testMpgApiPost()
    	{
    		$response = $this->post('/api/SpgatewayMPG/mpg/' .str_random(3). '/' .$this->faker()->randomDigitNotNull. '/' .$this->faker()->name. '/' .$this->faker()->email. '/');

    		$response->assertStatus(405);
    	}

    /**
     * 檢查mpg api格式
     *
     * @return void
     */
    public function testMpgApiget()
    {
    	$response = $this->get('/api/SpgatewayMPG/mpg/' .str_random(3). '/' .$this->faker()->randomDigitNotNull. '/' .$this->faker()->name. '/' .$this->faker()->email. '/');

    	$response->assertStatus(200);
    }


    /**
     * 檢查mpgFeedback api格式
     *
     * @return void
     */
 		public function testMpgFeedbackApiPost()
 		{
            $response = $this->get('/api/SpgatewayMPG/mpg/' .str_random(3). '/' .str_random(3). '/' .$this->faker()->name. '/' .$this->faker()->email. '/');
 			$response->assertStatus(200);
 		}

    /**
     * 檢查mpgFeedback Web格式
     *
     * @return void
     */
    public function testMpgFeedbackWebPost()
    {
    	$response = $this->post('/SpgatewayMPG/mpgFeedback/');
    	$response->assertStatus(200);
    }

    /**
     * 檢查mpgFeedback api格式
     *
     * @return void
     */
    public function testMpgFeedbackApiget()
    {
        $response = $this->get('/SpgatewayMPG/mpgFeedback/');
        $response->assertStatus(200);
    }



  }
