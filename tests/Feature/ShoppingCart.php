<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ShoppingCart extends TestCase
{

	use WithFaker;
	private $ordersArray = array('results' => array(
		'firstName'           => 'firstName',
		'lastName'            => 'lastName',
		'name'                => 'name',
		'recipient'           => 'recipient',
		'email'               => 'email',
		'tel'                 => 'tel',
		'shipping'            => 1,
		'address'             => 'address',
		'storeSelect'         => 0,
		'receipt'             => 1,
		'receiptTitle'        => 'receiptTitle',
		'receiptTax'          => 'receiptTax',
		'requestPaperInvoice' => false,
		'comment'             => 'comment',
		'cartContent'         => '{}',
		'subTotal'            => 0,
	));


		/**
     * 測試 createOrders api格式
     *
     * @return void
     */
		public function testcreateOrdersApiPost()
		{
			$response = $this->json('POST', '/api/shoppingCart/createOrders/', $this->ordersArray);
			$response->assertStatus(200);
		}


		/**
     * 測試 createOrders api格式
     *
     * @return void
     */
		public function testcreateOrdersApiGet()
		{
			$response = $this->get('/api/shoppingCart/createOrders/');
			$response->assertStatus(405);
		}

	}
