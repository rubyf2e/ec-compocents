<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AppfeedbackTest extends TestCase
{

    /**
     * 檢查api格式
     *
     * @return void
     */
    public function testApiPost()
    {
        $response = $this->post('/api/appfeedback/create');

        $response->assertStatus(200);
    }

    /**
     * 檢查api格式
     *
     * @return void
     */
       public function testApiget()
       {
        $response = $this->get('/api/appfeedback/create');

        $response->assertStatus(405);
    }

}
