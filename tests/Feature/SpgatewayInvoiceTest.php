<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SpgatewayInvoiceTest extends TestCase
{
	use WithFaker;
    private $MerchantOrderNo     = 'test12';
    private $Amt                 = '100';
    private $InvoiceNumber       = 'AA00000006';
    private $InvoiceTransNo      = '18012316094394499';
    private $AllowanceNo         = 'A180123161530805';
    private $AllowanceIssueArray = array(
        'ItemName'   => '',
        'ItemCount'  => '',
        'ItemUnit'   => '',
        'ItemPrice'  => '',
        'ItemAmt'    => '',
        'ItemTaxAmt' => '',
        'TotalAmt'   => '',
    );

    private $PostData_array = array(
        'type'         => '2',
        'BuyerName'    => '',
        'BuyerAddress' => '',
        'BuyerEmail'   => '',
        'BuyerPhone'   => '',
        /*銷售額 未稅*/
        'Amt'          => '',
        
        /*稅額*/
        'TaxAmt'       => '',
        
        /*總計           = 銷售額+稅額*/
        'TotalAmt'     => '',
        
        'ItemName'     => '',
        'ItemCount'    => '',
        'ItemUnit'     => '',
        
        /*單價           = 銷售額+稅額*/
        'ItemPrice'    => '',
        
        /*金額           = 銷售額+稅額*/
        'ItemAmt'      => '',
        
        'Comment'      => '',
    );


  	/**
     * 檢查checkData api格式
     *
     * @return void
     */
  	public function testCheckDataApiPost()
  	{
  		$response = $this->post('/api/SpgatewayInvoice/checkData/' .$this->MerchantOrderNo. '/' .$this->Amt. '/' .$this->InvoiceNumber. '/');

  		$response->assertStatus(405);
  	}


    /**
     * 檢查checkData api格式
     *
     * @return void
     */
    public function testCheckDataApiget()
    {
    	$response = $this->get('/api/SpgatewayInvoice/checkData/' .$this->MerchantOrderNo. '/' .$this->Amt. '/' .$this->InvoiceNumber. '/');

    	$response->assertStatus(200);
    }

    /**
     * 檢查invoice_issue api格式
     *
     * @return void
     */
    public function testInvoice_issueApiPost()
    {
        $response = $this->json('POST', '/api/SpgatewayInvoice/invoice_issue/' .$this->MerchantOrderNo, $this->PostData_array);
        $response->assertStatus(200);
    }

    /**
     * 檢查invoice_issue api格式
     *
     * @return void
     */
    public function testInvoice_issueApiget()
    {
    	$response = $this->get('/api/SpgatewayInvoice/invoice_issue/' .$this->MerchantOrderNo);

    	$response->assertStatus(405);
    }


    /**
     * 檢查invoice_touch_issue api格式
     *
     * @return void
     */
    public function testInvoice_touch_issueApiPost()
    {
    	$response = $this->post('/api/SpgatewayInvoice/invoice_touch_issue/' .$this->MerchantOrderNo. '/' .$this->InvoiceTransNo. '/' .$this->Amt. '/');
    	$response->assertStatus(405);
    }


    /**
     * 檢查invoice_touch_issue api格式
     *
     * @return void
     */
    public function testInvoice_touch_issueApiGet()
    {
    	$response = $this->get('/api/SpgatewayInvoice/invoice_touch_issue/' .$this->MerchantOrderNo. '/' .$this->InvoiceTransNo. '/' .$this->Amt. '/');
    	$response->assertStatus(200);
    }


    /**
     * 檢查invoice_invalid api格式
     *
     * @return void
     */
    public function testInvoice_invalidApiPost()
    {
    	$response = $this->post('/api/SpgatewayInvoice/invoice_invalid/' .$this->InvoiceNumber. '/test/');
    	$response->assertStatus(405);
    }


    /**
     * 檢查invoice_invalid api格式
     *
     * @return void
     */
    public function testInvoice_invalidApiGet()
    {
    	$response = $this->get('/api/SpgatewayInvoice/invoice_invalid/' .$this->InvoiceNumber. '/test/');
    	$response->assertStatus(200);
    }


 		/**
     * 檢查allowance_issue api格式
     *
     * @return void
     */
 		public function testAllowance_issueApiPost()
 		{
 			$response = $this->json('POST', '/api/SpgatewayInvoice/allowance_issue/' .$this->MerchantOrderNo. '/' .$this->InvoiceNumber, $this->AllowanceIssueArray);
 			$response->assertStatus(200);
 		}


    /**
     * 檢查allowance_issue api格式
     *
     * @return void
     */
    public function testAllowance_issueApiGet()
    {
    	$response = $this->get('/api/SpgatewayInvoice/allowance_issue/' .$this->MerchantOrderNo. '/' .$this->InvoiceNumber);
    	$response->assertStatus(405);
    }


 	/**
     * 檢查allowance_touch_issue api格式
     *
     * @return void
     */
     public function testAllowance_touch_issueApiPost()
     {
        $response = $this->post('/api/SpgatewayInvoice/allowance_touch_issue/C/' .$this->MerchantOrderNo. '/' .$this->AllowanceNo. '/' .$this->Amt);
        $response->assertStatus(200);
    }


    /**
     * 檢查allowance_touch_issue api格式
     *
     * @return void
     */
    public function testAllowance_touch_issueApiGet()
    {
    	$response = $this->get('/api/SpgatewayInvoice/allowance_touch_issue/C/' .$this->MerchantOrderNo. '/' .$this->AllowanceNo. '/' .$this->Amt);
    	$response->assertStatus(405);
    }


}
