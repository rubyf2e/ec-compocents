<?php

namespace Tests\Browser;

use Tests\TestCase;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class SpgatewayMPGTest extends DuskTestCase
{
	use WithFaker;
	private $MerchantOrderNo = '100013ddd333';
	private $Amt             = '10000';

    /**
     * 測試 mpg 是否可正確導到https://ccore.spgateway.com/MPG/mpg_gateway 頁面
     *
     * @return void
     */
    public function testMpgApiget()
    {
        $this->browse(function ($browser) {
            $browser->visit('/SpgatewayMPG/mpg/11111/11111/11111/11111@gmail.com')
            ->press('Submit')
            ->assertSee('請再次確認您的「訂單資訊」及「付款資訊」，付款完成後智付通將發送通知信至您的E-mail信箱');
        });
    }

}
